﻿using ProjectManager.Enums;
using ProjectManager.Objects;
using ProjectManager.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectManager
{
    public partial class Main : Form
    {
        public readonly ContextService _dataService;
        public readonly ProjectService _projectService;
        public readonly ImageService _imageService;
        public readonly LoggerService _loggerService;
        public readonly PageService _pageService;
        public readonly ReportService _reportService;
        //TODO - Generic methods xml needs to save nested lists / SQLite
        //TODO - Action responce - UI.
        //TODO - Add tools like 'find projects / get repositories'
        public Main()
        {
            InitializeComponent();
            _dataService = new ContextService();
            _projectService = new ProjectService(_dataService, _projectService);
            _imageService = new ImageService();
            _loggerService = new LoggerService();
            _pageService = new PageService();
            _reportService = new ReportService(_dataService, _projectService);
        }


        #region initPages
        private void Form1_Load(object sender, EventArgs e)
        {

            _pageService.AddPage(
                 new List<Page>() {

                    //page panels
                    new Page("Settings","Settings Page","Settings",pSetting,PanelTypeEnum.MainPanel,false),
                    new Page("Home","Home Page","Home",pHome,PanelTypeEnum.MainPanel,false),
                    new Page("Project","Project Page","Project",pProject,PanelTypeEnum.MainPanel,false),
                    new Page("Data","Data Page","Data",pData,PanelTypeEnum.MainPanel,false),
                    new Page("Reports","Reports Page","Reports",pReports,PanelTypeEnum.MainPanel,false),
                    new Page("Tools","Tools Page","Tools",pTools,PanelTypeEnum.MainPanel,false),

                    //constant panels
                    new Page("Side","Side Panel","Panel",pSideBar,PanelTypeEnum.SidePanel,true),
                    new Page("Header","Heaader Panel","Header",pHeader,PanelTypeEnum.HeaderPanel,true),

                    //Sub panels
                    new Page("","","",subpnlColour,PanelTypeEnum.SubPanel,true),
                    new Page("","","",subpnlSettingAccount,PanelTypeEnum.SubPanel,true),
                    new Page("","","",subpnlImport,PanelTypeEnum.SubPanel,true),
                    new Page("","","",subpnlExport,PanelTypeEnum.SubPanel,true),
                    new Page("","","",subpnlHome1,PanelTypeEnum.SubPanel,true),
                    new Page("","","",subpnlHome2,PanelTypeEnum.SubPanel,true),
                    new Page("","","",subpnlHome3,PanelTypeEnum.SubPanel,true),
                    new Page("","","",pnlDataSourceSettings,PanelTypeEnum.SubPanel,true),
                    new Page("","","",subpnlImortDirectory,PanelTypeEnum.SubPanel,true),

                    new Page("","","",subpnlTools,PanelTypeEnum.SubPanel,true),
                    new Page("","","",subpnlHome,PanelTypeEnum.SubPanel,true),
                    new Page("","","",subpnlReport,PanelTypeEnum.SubPanel,true),


                });

            //show home page
            btnHome.PerformClick();

            //settings
            initSettings();

            //pages
            initPages();

            //begin logging
            _loggerService.Create();
            _loggerService.Log(new Logger("-----START APPLICATION-----"));



        }

        private void initReports()
        {
            _reportService.AddReports();

            foreach (var report in _reportService.getReports())
            {
                lstReport.Items.Add(report);
            }

            cboReportDataSource.Items.AddRange(Enum.GetNames(typeof(DataSourceTypeEnum)));
        }

        private void initPages()
        {
            //get projetcs
            List<Project> AllProjects = _projectService.GetAll(typeof(Project));

            //add severs, repos, jobs etc.. to project object
            PopulateProjectObjects(AllProjects);

            //setup project page
            initProject(AllProjects);

            //setup data page
            initData(AllProjects);

            //setup reports page
            initReports();
        }

        private void PopulateProjectObjects(List<Project> AllProjects)
        {
            List<Repository> AllRepository = _projectService.GetAll(typeof(Repository));
            List<Server> AllServer = _projectService.GetAll(typeof(Server));
            List<Job> AllJob = _projectService.GetAll(typeof(Job));
            List<Settings> AllSettings = _projectService.GetAll(typeof(Settings));

            //8) get all of the objects for each project
            foreach (Project p in AllProjects)
            {
                if (p.Name == "None")
                    continue;

                p.Repositories = new List<Repository>();
                p.Servers = new List<Server>();
                p.Jobs = new List<Job>();

                foreach (Repository r in AllRepository)
                {
                    if (p.RepositoryIds.Contains(r.Id))
                    {
                        p.Repositories.Add(r);
                    }
                }
                foreach (Server r in AllServer)
                {
                    if (p.ServerIds.Contains(r.Id))
                    {
                        p.Servers.Add(r);
                    }
                }
                foreach (Job r in AllJob)
                {
                    if (p.JobIds.Contains(r.Id))
                    {
                        p.Jobs.Add(r);
                    }
                }
            }
        }

        private void initProject(List<Project> AllProjects)
        {
            cboProject.DataSource = AllProjects.ToList();
            if (AllProjects.Where(x => x.Name == "None").ToList().Count == 0)
            {
                AllProjects.Add(new Project() { Name = "None", ProjectId = -1 });
            }
            cboSortBy.DataSource = new string[] { typeof(Project).Name, typeof(Server).Name, typeof(Job).Name, typeof(Repository).Name };
            dgvProjectsTab.DataSource = AllProjects;

            //id column is read only
            dgvProjectsTab.Columns[0].ReadOnly = true;
        }

        private void initData(List<Project> allProjects)
        {
            cboDataType.DataSource = new string[] { "Whole Project", "Server", "Repository", "Job", "Note" };
            cboDataFormat.DataSource = Enum.GetNames(typeof(DataSourceTypeEnum));
            cboDataProject.DataSource = allProjects;
            btnImportFinal.Enabled = false;
        }

        private void initSettings()
        {
            //1) if we have settings populate that list first 
            List<Settings> hasSettings = _projectService.Load(typeof(Settings), Consts.Settings, DataSourceTypeEnum.JSON);

            //2) pre-set the datasource option in case this is a first time build
            var datasource = DataSourceTypeEnum.JSON;

            //7) set setting panel data source options
            var list = Enum.GetNames(typeof(DataSourceTypeEnum));
            foreach (var str in list)
            {
                cboDatasourceType.Items.Add(str);
            }

            //3) set the saved data source if set
            if (hasSettings.Count >= 0)
            {
                _projectService.Populate(typeof(Settings), Consts.Settings, DataSourceTypeEnum.JSON);

                datasource = _projectService.GetDataSource();

                //4) set all of the other settings.
                foreach (Settings setting in _projectService.GetAll(typeof(Settings)))
                {
                    if (setting.settingType == "Main")
                    {

                        //set the settings
                        lblUserName.Text = setting.accountName;

                        if (setting.accountImage != null && setting.accountImage != "")
                        {
                            pUserImage.BackgroundImage = Image.FromFile(setting.accountImage);
                        }
                        _pageService.ColourPages(setting);

                        //set the settings of the settings panel..
                        filename = setting.accountImage;
                        txtTitleColour.Text = setting.titleColour;
                        txtSideBarColour.Text = setting.sidebarColour;
                        txtPanelPrimary.Text = setting.panelColourPrimary;
                        txtPanelSecondary.Text = setting.panelColourSecond;
                        txtAccountName.Text = setting.accountName;
                        cboDatasourceType.SelectedIndex = cboDatasourceType.Items.IndexOf(setting.datasource);
                    }
                }
            }



            //4) create all data source files
            _projectService.Create(_dataService.SeedTestDataRepository(), Consts.DataSource, datasource);
            _projectService.Create(_dataService.SeedTestDataServer(), Consts.DataSource, datasource);
            _projectService.Create(_dataService.SeedTestDataJobs(), Consts.DataSource, datasource);
            _projectService.Create(_dataService.SeedTestDataProjects(), Consts.DataSource, datasource);

            //5) populate all data source lists from file 
            _projectService.Populate(typeof(Job), Consts.DataSource, datasource);
            _projectService.Populate(typeof(Repository), Consts.DataSource, datasource);
            _projectService.Populate(typeof(Server), Consts.DataSource, datasource);
            _projectService.Populate(typeof(Project), Consts.DataSource, datasource);

            _projectService.Backup(_projectService.GetAll(typeof(Project)), Consts.BackupDirectory, datasource);
            _projectService.Backup(_projectService.GetAll(typeof(Server)), Consts.BackupDirectory, datasource);
            _projectService.Backup(_projectService.GetAll(typeof(Job)), Consts.BackupDirectory, datasource);
            _projectService.Backup(_projectService.GetAll(typeof(Repository)), Consts.BackupDirectory, datasource);
        }
        #endregion

        #region PageManager Methods
        private void btnTools_Click(object sender, EventArgs e)
        {
            _pageService.Show(pTools, lblHeaderTitle);
            txtAdvice.Text = "Advice: Tools to make life easier!";  
        }
        private void btnHome_Click(object sender, EventArgs e)
        {
            _pageService.Show(pHome, lblHeaderTitle);
            txtAdvice.Text = "Advice: Select a menu option!";
        }

        private void btnProjects_Click(object sender, EventArgs e)
        {
            _pageService.Show(pProject, lblHeaderTitle);
            txtAdvice.Text = "Advice: Filter on all project data!";
        }

        private void btnData_Click(object sender, EventArgs e)
        {
            _pageService.Show(pData, lblHeaderTitle);
            txtAdvice.Text = "Advice: Export or Import new data!";
        }
        private void lblData_Click(object sender, EventArgs e)
        {
            _pageService.Show(pData, lblHeaderTitle);
            txtAdvice.Text = "Advice: Export or Import new data!";
        }
        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            _pageService.Show(pData, lblHeaderTitle);
            txtAdvice.Text = "Advice: Export or Import new data!";
        }
        private void pSettings_Click(object sender, EventArgs e)
        {
            _pageService.Show(pSetting, lblHeaderTitle);
            txtAdvice.Text = "Advice: Give me a new style ;)!";
        }
        private void btnReport_Click(object sender, EventArgs e)
        {
            _pageService.Show(pReports, lblHeaderTitle);
            txtAdvice.Text = "Advice: Custom reports based of the dataset!";
        }
        private void subpnlHome2_Click(object sender, EventArgs e)
        {
            _pageService.Show(pData, lblHeaderTitle);
            txtAdvice.Text = "Advice: Export or Import new data!";
        }
        private void pProjects_Click(object sender, EventArgs e)
        {
            _pageService.Show(pProject, lblHeaderTitle);
            txtAdvice.Text = "Advice: Filter on all project data!";
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            _pageService.Show(pProject, lblHeaderTitle);
            txtAdvice.Text = "Advice: Filter on all project data!";
        }
        private void subpnlHome1_Click(object sender, EventArgs e)
        {
            _pageService.Show(pProject, lblHeaderTitle);
            txtAdvice.Text = "Advice: Filter on all project data!";
        }
        #endregion

        #region Project Panel
        //update projects
        private void dgvProjectsTab_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            dynamic aStudent = (dynamic)dgvProjectsTab.CurrentRow.DataBoundItem;
            var result = _projectService.getById(aStudent);
            _projectService.UpdateProject(result, aStudent);
            _projectService.Save(_projectService.GetAll(aStudent.GetType()), Consts.DataSource, _projectService.GetDataSource());
        }

        //filter projects
        private void cboProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            Project project = (Project)cboProject.SelectedItem as Project;
            dgvProjectsTab.DataSource = _projectService.searchFor(cboSortBy.SelectedIndex, txtSearch.Text, project.ProjectId);
        }
        private void cboSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            Project project = (Project)cboProject.SelectedItem as Project;
            dgvProjectsTab.DataSource = _projectService.searchFor(cboSortBy.SelectedIndex, txtSearch.Text, project.ProjectId);
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            Project project = (Project)cboProject.SelectedItem as Project;
            dgvProjectsTab.DataSource = _projectService.searchFor(cboSortBy.SelectedIndex, txtSearch.Text, project.ProjectId);
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            Project project = (Project)cboProject.SelectedItem as Project;
            dgvProjectsTab.DataSource = _projectService.searchFor(cboSortBy.SelectedIndex, txtSearch.Text, project.ProjectId);
        }
        #endregion

        #region Data Panel

        public static dynamic ImportedData = "";
        public static dynamic deserialisedData = "";

        private void btnExport_Click(object sender, EventArgs e)
        {
            var type = cboDataType.SelectedItem.ToString();
            var project = cboDataProject.SelectedItem.ToString();
            DataSourceTypeEnum format;
            Enum.TryParse(cboDataFormat.SelectedItem.ToString(), out format);
            string projects = "";
            string saveformat = _dataService.getSaveFormat(format);
            List<Project> productList = _projectService.GetAll(typeof(Project));
            List<Project> productListFiltered = productList.Where(m => m.Name == project).ToList();
            switch (type)
            {
                case "Server":
                    projects = _dataService.SerializeTo(productListFiltered.SelectMany(b => b.Servers).Distinct().ToList() as List<Server>, format);
                    break;
                case "Repository":
                    projects = _dataService.SerializeTo(productListFiltered.SelectMany(b => b.Repositories).Distinct().ToList() as List<Repository>, format);
                    break;
                case "Job":
                    projects = _dataService.SerializeTo(productListFiltered.SelectMany(b => b.Jobs).Distinct().ToList() as List<Job>, format);
                    break;
                case "Whole Project":
                    projects = _dataService.SerializeTo(productListFiltered, format);
                    break;
            }


            if (rdoExportToText.Checked)
            {
                txtExport.Text = projects;
            }
            else if (rdoExportToFile.Checked)
            {
                SaveFileDialog save = new SaveFileDialog
                {
                    Filter = saveformat,
                    FilterIndex = 2,
                    RestoreDirectory = true
                };
                if (save.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter writer = new StreamWriter(save.OpenFile()))
                    {
                        if (save.FileName != "")
                        {
                            writer.Write(projects);
                            writer.Flush();
                        }
                    }
                }
                else
                {
                    //TODO: extend exception class to create custom eror meszsage message.
                }

            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {

            _loggerService.Log(new Logger("Started Import Process"));


            FileInfo file;
            OpenFileDialog folderBrowser = new OpenFileDialog();
            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;
            folderBrowser.FileName = "Folder Selection.";

            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {

                _loggerService.Log(new Logger($"     Selected File: {folderBrowser.FileName}"));

                //save file
                file = new FileInfo(folderBrowser.FileName);

                //dynamic data variables
                dynamic DeserializedData = null;
                dynamic DeserializeDataType = null;

                //classify the datatype by file name
                if (file.FullName.ToLower().Contains("server"))
                {
                    DeserializeDataType = new Server();
                    _loggerService.Log(new Logger($"     Import Data Type Is: Server"));
                }
                else if (file.FullName.ToLower().Contains("repository"))
                {
                    DeserializeDataType = new Repository();
                    _loggerService.Log(new Logger($"     Import Data Type Is: Repository"));
                }
                else if (file.FullName.ToLower().Contains("job"))
                {
                    DeserializeDataType = new Job();
                    _loggerService.Log(new Logger($"     Import Data Type Is: Job"));
                }
                else if (file.FullName.ToLower().Contains("Project"))
                {
                    _loggerService.Log(new Logger($"     Import Data Type Is: Project"));
                    DeserializeDataType = new Project();

                }

                //deserialise data into a dynamic object
                switch (file.Extension.ToLower())
                {
                    case ".csv":
                        DeserializedData = _dataService.DeserializeTo(DeserializeDataType, file.FullName, DataSourceTypeEnum.CSV);
                        break;
                    case ".sql":
                        DeserializedData = _dataService.DeserializeTo(DeserializeDataType, file.FullName, DataSourceTypeEnum.SQLITE);
                        break;
                    case ".json":
                        DeserializedData = _dataService.DeserializeTo(DeserializeDataType, file.FullName, DataSourceTypeEnum.JSON);
                        break;
                    case ".xml":
                        DeserializedData = _dataService.DeserializeTo(DeserializeDataType, file.FullName, DataSourceTypeEnum.XML);
                        break;
                }

                _loggerService.Log(new Logger($"     File Extenstion is: {file.Extension}"));



                //get inital data to compare against
                var data = _projectService.GetAll(DeserializeDataType.GetType());

                //produce file info
                if (Consts.ShowFileInfo)
                {
                    lstFileInfo.Items.AddRange(GetFileInfo(file));
                    _loggerService.Log(new Logger($"     Displayed file info."));

                }

                //get data info
                if (Consts.ShowDataInfo)
                {
                    lstFileInfo.Items.AddRange(GetDataInfo(DeserializedData, DeserializeDataType));
                    _loggerService.Log(new Logger($"     Displayed data info."));
                }


                ImportedData = data;
                deserialisedData = DeserializedData;
                btnImportFinal.Enabled = true;

                if (ImportedData != null && deserialisedData != null)
                {
                    btnImportFinal.Enabled = true;
                    _loggerService.Log(new Logger($"     Enabling import final button."));
                }

            }
        }
        private object[] GetDataInfo(dynamic deserializedData, dynamic deserializeDataType)
        {

            string duplicateObjects = "";
            string totalObjectsInUpload = "";
            string totalObjectsIfUploaded = "";
            string UniqueObjects = "";
            var data = _projectService.GetAll(deserializeDataType.GetType()) as List<Server>;

            if (deserializedData != null)
            {
                //toal objects within the file uploaded
                totalObjectsInUpload = $"Total Uploaded {deserializedData[0].GetType().Name}(s) In Upload: {deserializedData.Count}";

                //total objects when added to the total number of objects within the datasource. (forshadow after upload for user.)
                totalObjectsIfUploaded = $"Total {deserializedData[0].GetType().Name}(s) After Upload: {(deserializedData).Count + (data).Count}";

                //total number of duplicates
                int dups = 0;
                foreach (var obj in deserializedData)
                {
                    //if the data we have already contains an element we are uploading 
                    //TODO: Make more accurate by checking for ID's find cleaner way todo this in gerneral.
                    if (_projectService.isDuplicate(obj))
                    {
                        dups += 1;
                    }
                }
                duplicateObjects = $"Duplicate {deserializedData[0].GetType().Name}(s) Found: {dups.ToString()}";

                //total number of unique uploads that the system does not recognize
                int unique = 0;
                foreach (var obj in deserializedData)
                {
                    //if the data we have already contains any element we are uploading 
                    //TODO: Make more accurate by checking for ID's find cleaner way todo this in gerneral.
                    if (!_projectService.isDuplicate(obj))
                    {
                        unique += 1;
                    }
                }
                UniqueObjects = $"Unique {deserializedData[0].GetType().Name}(s) Found: {unique.ToString()}";



            }
            return new string[]
            {
                totalObjectsInUpload,
                totalObjectsIfUploaded,
                duplicateObjects,
                UniqueObjects
            };
        }
        private string[] GetFileInfo(FileInfo file)
        {
            return new string[] {
                "Path: "+file.FullName,
                "File Name: "+file.Name,
                "Size: "+file.Length+"KB",
                "File Name: "+file.CreationTime.ToString("dd MM yy"),
                ""
            };


        }
        private void btnImportFinal_Click(object sender, EventArgs e)
        {
            //TODO: Run some checks here.
            _projectService.Backup(ImportedData, Consts.BackupDirectory, _projectService.GetDataSource());
            _loggerService.Log(new Logger("     Backed up current objects"));
            _projectService.AddObjects(deserialisedData);
            _loggerService.Log(new Logger("     Adding imported objects to datasource"));
            _projectService.Save(ImportedData, Consts.DataSource);
            _loggerService.Log(new Logger("     Saved changes!"));
            clearImportForm();
            giveImportNotification();
            _loggerService.Log(new Logger("Import Process Complete!"));
        }

        private void giveImportNotification()
        {
            MessageBox.Show(this, "The data has been added successfully.", "Import successful!");
        }

        private void clearImportForm()
        {
            //clear out global variables
            ImportedData = "";
            deserialisedData = "";

            //clear textbox
            lstFileInfo.Items.Clear();

            //set impoty button to disabled
            btnImportFinal.Enabled = false;
        }
        #endregion

        #region Settings Panel
        private static string filename = "";
        private void SelectAccountImage_Click(object sender, EventArgs e)
        {
            lblImageName.Text = "";

            OpenFileDialog of = new OpenFileDialog
            {
                //For any other formats
                Filter = "Image Files (*.bmp;*.jpg;*.jpeg,*.png)|*.BMP;*.JPG;*.JPEG;*.PNG"
            };
            if (of.ShowDialog() == DialogResult.OK)
            {
                var image = _imageService.GetThumbnail(new FileInfo(of.FileName), pUserImage.Size.Width, pUserImage.Size.Height, "", false);
                filename = @"" + image;
                pUserImage.BackgroundImage = Image.FromFile(@"" + image);
                pUserImage.Update();

            }
        }

        private void btnSaveAccount_Click(object sender, EventArgs e)
        {
            //1) grab image and name

            //2) check to see if previous settings exist
            var settingsList = _projectService.GetAll(typeof(Settings));

            //2)  create settings directory
            if (!Directory.Exists(Consts.Settings))
            {
                Directory.CreateDirectory(Consts.Settings);
            }

            //3) loop over settings if exist
            if (settingsList.Count > 0)
            {
                foreach (Settings prop in settingsList)
                {
                    //if this is the core settings object
                    if (prop.settingType == "Main")
                    {
                        //update the name if required
                        prop.accountName = txtAccountName.Text;

                        //update the image if required
                        if (filename.Length > 0)
                        {
                            prop.accountImage = filename;
                        }
                        prop.titleColour = txtTitleColour.Text;
                        prop.sidebarColour = txtSideBarColour.Text;
                        prop.panelColourPrimary = txtPanelPrimary.Text;
                        prop.panelColourSecond = txtPanelSecondary.Text;
                        prop.datasource = cboDatasourceType.SelectedItem.ToString();
                    }

                }
            }
            else
            {
                settingsList.Add(new Settings()
                {

                    //update the name if required
                    accountName = txtAccountName.Text,
                    accountImage = filename,
                    titleColour = txtTitleColour.Text,
                    sidebarColour = txtSideBarColour.Text,
                    panelColourPrimary = txtPanelPrimary.Text,
                    panelColourSecond = txtPanelSecondary.Text,
                    datasource = cboDatasourceType.SelectedItem.ToString()
                });
            }

            //3)save listS
            _projectService.Save(settingsList, Consts.Settings, DataSourceTypeEnum.JSON);

        }
        private void btnTestColours_Click(object sender, EventArgs e)
        {

            var settingViewModel = new Settings()
            {
                titleColour = txtTitleColour.Text,
                sidebarColour = txtSideBarColour.Text,
                panelColourPrimary = txtPanelPrimary.Text,
                panelColourSecond = txtPanelSecondary.Text

            };
            _pageService.ColourPages(settingViewModel);


        }
        #endregion

        #region Unused
        private void pHeader_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pSideBar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }
        private void pUserImage_Paint(object sender, PaintEventArgs e)
        {

        }
        private void lblUserName_Click(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {
        }
        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        private void dgvDataScreen_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
        private void cboFilter_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void cboDataProject_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void cboDataType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void cboDataFormat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void pProject_Paint(object sender, PaintEventArgs e)
        {

        }
        private void lstFileInfo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void pSettings_Paint(object sender, PaintEventArgs e)
        {

        }
        private void subpnlHome1_Paint(object sender, PaintEventArgs e)
        {

        }
        #endregion

        #region Report Panel

        private void btnExportReport_Click(object sender, EventArgs e)
        {
            var projects = "";
            Report report = lstReport.SelectedItem as Report;
            DataSourceTypeEnum format;
            Enum.TryParse(cboReportDataSource.SelectedItem.ToString(), out format);
            string saveformat = _dataService.getSaveFormat(format);
            if (report != null)
            {

                switch (report.report.ToList()[0].GetType().Name)
                {
                    case "Server":
                        var serverList = report.report as IEnumerable<Server>;
                        projects = _dataService.SerializeTo(serverList as List<Server>, format);
                        break;
                    case "Repository":
                        var repoList = report.report as IEnumerable<Repository>;
                        projects = _dataService.SerializeTo(repoList as List<Repository>, format);
                        break;
                    case "Job":
                        var JobList = report.report as IEnumerable<Job>;
                        projects = _dataService.SerializeTo(JobList as List<Job>, format);
                        break;
                    case "Project":
                        var projectList = (report.report as IEnumerable<Project>).ToList();
                        projects = _dataService.SerializeTo(projectList, format);
                        break;
                }

            }

            if (rdoReportExportText.Checked)
            {
                txtReportData.Text = projects;
            }
            else if (rdoReportExportFile.Checked)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = saveformat;
                save.FilterIndex = 2;
                save.RestoreDirectory = true;
                if (save.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter writer = new StreamWriter(save.OpenFile()))
                    {
                        if (save.FileName != "")
                        {
                            writer.Write(projects);
                            writer.Flush();
                        }
                    }
                }
                else
                {
                    //TODO: extend exception class to create custom eror meszsage message.
                }
            }
        }

        private void txtReportSearch_TextChanged(object sender, EventArgs e)
        {
            var list = _reportService.getReports();

            List<Report> templist = new List<Report>();
            if (txtReportSearch.Text.Length > 0)
            {
                foreach (var item in list)
                {
                    Report report = item as Report;
                    if (report.name.ToLower().Contains(txtReportSearch.Text.ToLower()))
                    {
                        templist.Add(report);

                    }
                }
                lstReport.DataSource = templist;
            }
            else
            {
                lstReport.DataSource = list;
            }
        }


        #endregion

        #region Tool Pannel
        private void btnFindDirectories_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    var Directories = Directory.EnumerateDirectories(fbd.SelectedPath);

                    if (Directories.Count() > 0)
                    {

                        List<Project> plist = new List<Project>();
                        var index = _projectService.maxId();

                        foreach (var dir in Directories)
                        {
                            var dirName = new DirectoryInfo(dir);
                            index++;
                            Project p = new Project()
                            {
                                Name = dirName.Name,
                                CreatedAt = dirName.CreationTime,
                                LastUpdated = DateTime.Now,
                                Company = dirName.Parent.Name,
                                ProjectId = index

                            };

                            plist.Add(p);
                        }

                        _projectService.AddObjects(plist);
                    }

                }
            }
        }


        #endregion



        private void subpnlHome2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pHome_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
