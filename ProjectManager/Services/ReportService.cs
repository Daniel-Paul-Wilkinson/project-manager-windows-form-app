﻿using ProjectManager.Interfaces;
using ProjectManager.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Services
{
    public class ReportService
    {

        private readonly IContext _dataService;
        private readonly IProjectService _ProjectService;
       

       public ReportService(IContext dataService, IProjectService projectService)
        {
            _dataService = dataService;
            _ProjectService = projectService;
        }

        public List<Report> reportList = new List<Report>();

        public void AddReport(Report p)
        {
            reportList.Add(p);
        }

        public void AddRange(List<Report> reports)
        {
            reportList.AddRange(reports);
        }

        public void AddReports()
        {


            var allServers = new Report()
            {
                name = "All Servers",
                report = _ProjectService.GetAll(typeof(Server)),
                date = DateTime.Now,
                order = 0,
            };
            var allRepository = new Report()
            {
                name = "All Repositorys",
                report = _ProjectService.GetAll(typeof(Repository)),
                date = DateTime.Now,
                order = 0,
            };
            var allJobs = new Report()
            {
                name = "All Jobs",
                report = _ProjectService.GetAll(typeof(Job)),
                date = DateTime.Now,
                order = 0,
            };
            var allProjects = new Report()
            {
                name = "All Projects",
                report = _ProjectService.GetAll(typeof(Project)),
                date = DateTime.Now,
                order = 0,
            };

            var allCompanyCourtaulds = new Report()
            {
                name = "All Courtauds Sites",
                report = _ProjectService.GetCompanyReport(typeof(Project), "Courtalds"),
                date = DateTime.Now,
                order = 0,
            };
            reportList.Add(allCompanyCourtaulds);
            reportList.Add(allServers);
            reportList.Add(allJobs);
            reportList.Add(allRepository);
            reportList.Add(allProjects);

        }


        public List<Report> getReports()
        {
            return reportList
                .OrderBy(x => x.order)
                .ToList();
        }
          

    }
}
