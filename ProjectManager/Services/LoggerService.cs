﻿using ProjectManager.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Services
{
    public class LoggerService
    {

        string path = "";

        public void Create()
        { 
            var fullname = Consts.LoggerDirectory + $"/" + Consts.LoggerFilename;

            if (!Directory.Exists(Consts.LoggerDirectory))
            {
                Directory.CreateDirectory(Consts.LoggerDirectory);
            }

            if (!File.Exists(fullname)) {
                File.Create(fullname);
                path = fullname;
            }
            else
            {
                path = Consts.LoggerDirectory + $"/" + Consts.LoggerFilename;
            }
        }
        public void Log(Logger log)
        {
            if (File.Exists(path))
            {
                File.AppendAllText(path, log.time + ": " + log.message + Environment.NewLine);
            }
      }
    }
}
