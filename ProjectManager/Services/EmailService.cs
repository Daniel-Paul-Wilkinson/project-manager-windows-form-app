﻿using ProjectManager.Enums;
using ProjectManager.Interfaces;
using ProjectManager.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Services
{
    public class EmailService : IEmailService
    {
        public void SendEmail(Email email)
        {
            //Build SMTP object
            SmtpClient smtp = new SmtpClient();
            smtp.UseDefaultCredentials = false;
            smtp.Host = email.Host;
            smtp.EnableSsl = true;
            smtp.Credentials = new NetworkCredential(email.Personal_Email, email.Personal_Email_Password);

            //build mail object
            MailMessage MailMessage = new MailMessage();
            MailMessage.From = new MailAddress(email.Personal_Email);

            //add all recipients
            foreach (string Recipient in email.Recipients)
            {
                MailMessage.To.Add(Recipient);
            }
            MailMessage.Body = email.Body;
            MailMessage.IsBodyHtml = email.isHTML;
            MailMessage.Subject = email.Subject;

            //send mail
            smtp.Send(MailMessage);
        }

        public Email createEmailBody(Email email, Project project, Job job, EmailTypeEnum emailType)
        {

            string emailPath = string.Empty;
            string tempBody = string.Empty;
            string tempTo = string.Empty;
            string tempLinks = string.Empty;

            switch (emailType)
            {
                case EmailTypeEnum.DeveloperLink:
                    emailPath = @"EmailTemps/EmailDevLink.html";
                    break;
                case EmailTypeEnum.JobInformation:
                    break;
                case EmailTypeEnum.ProjectInformation:
                    break;
                case EmailTypeEnum.ServerInformation:
                    break;
            }



            //read template
            using (StreamReader reader = new StreamReader(emailPath))
            {
                tempBody = reader.ReadToEnd();
            }

            //get user emails included
            foreach (string recipient in email.Recipients)
            {
                tempTo += recipient + ", ";
            }


            //add links to email
            foreach (var v in project.Jobs)
            {
                //foreach (KeyValuePair<string, string> link in v.URL)
                //{
                //    tempLinks += String.Format(Consts.EmailURLString, "", link.Key, link.Value) + "<br />";
                //}
            }

            //replace email content
            tempBody = tempBody.Replace("{message}", email.Message);
            tempBody = tempBody.Replace("{links}", tempLinks);
            tempBody = tempBody.Replace("{UserNames}", tempTo);


            email.isHTML = true;
            email.Body = tempBody;
            email.Subject = job.JobIdentifier + " " + project.Company + " - " + job.Name;
            return email;

        }
    }
}
