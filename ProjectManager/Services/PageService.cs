﻿using ProjectManager.Enums;
using ProjectManager.Objects;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectManager
{
    public class PageService
    {
        List<Page> Pages = new List<Page>();

        public void AddPage(List<Page> page)
        {
            Pages.AddRange(page);
        }

        public void Show(Panel panel,Label label)
        {
            var Page = Pages.Where(x => x.panel.Equals(panel)).FirstOrDefault();
            if (Page != null)
            {
                foreach (var pageItem in Pages)
                {
                    if (pageItem.Equals(Page) || pageItem.ActiveAlways)
                    {
                        pageItem.panel.Show();

                        //only update the panel title if its the main panel.
                        if (pageItem.PanelType == PanelTypeEnum.MainPanel)
                        {
                            label.Text = pageItem.name;
                        }
                    }
                    else
                    {
                        pageItem.panel.Hide();
                    }

                }
            }
        }
        public void ColourPages(Settings setting)
        {
            foreach (var page in Pages) {

                switch (page.PanelType)
                {
                    case PanelTypeEnum.HeaderPanel:
                        page.panel.BackColor = ColorTranslator.FromHtml(setting.titleColour);
                        break;
                    case PanelTypeEnum.MainPanel:
                        page.panel.BackColor = ColorTranslator.FromHtml(setting.panelColourPrimary);
                        break;
                    case PanelTypeEnum.SidePanel:
                        page.panel.BackColor = ColorTranslator.FromHtml(setting.sidebarColour);
                        break;
                    case PanelTypeEnum.SubPanel:
                        page.panel.BackColor = ColorTranslator.FromHtml(setting.panelColourSecond);
                        break;
                }

            }

        }
    }
}
