﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.Enums;
using ProjectManager.Objects;
using ProjectManager.Interfaces;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ProjectManager.Services
{
    public class ImageService
    {
        public string GetThumbnail(FileInfo source, int newWidth, int newHeight, string missingImageFile, bool fixMaxSize = false)
        {
            //1) Create directory for images if it does not exist
            var localSource = @"Images";

            if (!Directory.Exists(localSource))
            {
                Directory.CreateDirectory(localSource);
            }

            //2) Copy reference to the 'high resolution' image in case we need to make future thumbnails from this image. (could create this in a panel like CMS...)
            if (!File.Exists(localSource+"/"+ source.Name))
            {
                File.Copy(source.FullName,localSource +"/" +source.Name);
            }

            //3) create a new file name in the local source path.
            var localDestination = Path.Combine(localSource +"/"+ $"{newWidth}x{newHeight}" + Path.GetFileName(source.Name));

            //4) Resize image (if required)
            if (!File.Exists(localDestination))
            {
                if (fixMaxSize)
                    ResizeImageWithMaxSize(source.FullName, localDestination, newWidth, newHeight);
                else
                    ResizeImage(source.FullName, localDestination, newWidth, newHeight, missingImageFile);
            }

            //5) return the new path to the resized image.
            return $"{localDestination}";
        }
        public bool ResizeImage(string sourceFile, string destinationFile, int newWidth, int newHeight, string missingImageFile)
        {
            if (!File.Exists(sourceFile)) return false;


            var sourceImage = Image.FromFile(sourceFile);
            var sourceRect = new Rectangle(0, 0, sourceImage.Width, sourceImage.Height);

            var scaleHeight = sourceImage.Height * newWidth / sourceImage.Width;
            var scaleWidth = sourceImage.Width * newHeight / sourceImage.Height;

            Rectangle destinationRect;
            Bitmap bitImage;

            if (newWidth > scaleWidth)
            {
                // Scale by Height
                destinationRect = new Rectangle(0, 0, scaleWidth, newHeight);
                bitImage = new Bitmap(scaleWidth, newHeight);
            }
            else
            {
                // Scale by Width
                destinationRect = new Rectangle(0, 0, newWidth, scaleHeight);
                bitImage = new Bitmap(newWidth, scaleHeight);
            }

            var graphics = Graphics.FromImage(bitImage);
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.DrawImage(sourceImage, destinationRect, sourceRect, GraphicsUnit.Pixel);

            bitImage.Save(destinationFile);

            bitImage.Dispose();
            graphics.Dispose();
            sourceImage.Dispose();

            return true;
        }

        public bool ResizeImageWithMaxSize(string sourceFile, string destinationFile, int newWidth, int newHeight)
        {
            if (!File.Exists(sourceFile)) return false;

            var sourceImage = Image.FromFile(sourceFile);
            var sourceRect = new Rectangle(0, 0, sourceImage.Width, sourceImage.Height);

            int width;
            int height;

            if ((newHeight == 0) && (newWidth != 0))
            {
                width = newWidth;
                height = sourceImage.Size.Height * width / sourceImage.Size.Width;
            }
            else if ((newHeight != 0) && (newWidth == 0))
            {
                height = newHeight;
                width = sourceImage.Size.Width * height / sourceImage.Size.Height;
            }
            else
            {
                width = newWidth;
                height = newHeight;
            }

            var destinationRect = new Rectangle(0, 0, width, height);
            var bitImage = new Bitmap(width, height);
            var graphics = Graphics.FromImage(bitImage);

            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.DrawImage(sourceImage, destinationRect, sourceRect, GraphicsUnit.Pixel);

            bitImage.Save(destinationFile);

            bitImage.Dispose();
            graphics.Dispose();
            sourceImage.Dispose();

            return true;
        }
    }
}
