﻿using Newtonsoft.Json;
using ProjectManager.Enums;
using ProjectManager.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ProjectManager.Interfaces;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.Security.Permissions;
using System.Data.SQLite;
using System.Windows.Input;
using System.Xml.Serialization;
using System.Reflection;
using ProjectManager.Extensions;

namespace ProjectManager
{
    public class ContextService : IContext
    {

        #region Cruid methods
        public dynamic Load<T>(T t, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON) where T : Type
        {
            var i = getObjectFromType(t);
            string filePath = dataSource + "/" + GetFileExtension(dataSource, t.Name, dataSourceType);
            bool fileExists = File.Exists(filePath) || (File.Exists(dataSource + "/" + Consts.SQLiteFile)) ? true : false;
            List<T> data = null;

            if (fileExists)
            {
                switch (dataSourceType)
                {
                    case DataSourceTypeEnum.CSV:
                        return DeserializeFromCSV(i, filePath);
                    case DataSourceTypeEnum.XML:
                        var rsult = DeserializeFromXML(i, filePath);
                        return DeserializeFromXML(i, filePath);
                    case DataSourceTypeEnum.JSON:
                        return DeserializeFromJSON(i, filePath);
                    case DataSourceTypeEnum.SQLITE:
                        return DeserializeFromSQL(i, dataSource + "/" + Consts.SQLiteFile);
                }
            }
            else { return null; }
            return data as List<T>;
        }
        public bool Create<T>(List<T> t = null, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            //variables
            bool Exists = false;

            //if directory exists
            if (!Directory.Exists(dataSource))
            {
                Directory.CreateDirectory(dataSource);
            }

            Type GenericType = getObjectType(t);

            //get the file path
            string filePath = dataSource + "/" + GetFileExtension(dataSource, GenericType.Name, dataSourceType);

            //if the file does not exist
            if (!File.Exists(filePath))
            {
                switch (dataSourceType)
                {
                    case DataSourceTypeEnum.JSON:
                        File.WriteAllText(filePath, SerializeToJSON(t));
                        Exists = true;
                        break;
                    case DataSourceTypeEnum.XML:
                        File.WriteAllText(filePath, SerializeToXML<List<T>>(t));
                        Exists = true;
                        break;
                    case DataSourceTypeEnum.CSV:
                        File.WriteAllText(filePath, SerializeToCSV(t));
                        Exists = true;
                        break;
                    //Create SQL tables and insert the first lot of data
                    case DataSourceTypeEnum.SQLITE:

                        //set the file path again for SQL as we need a seperate naming convention
                        filePath = dataSource + "/" + Consts.SQLiteFile;

                        //build up connection string object
                        SQLiteConnection sQLiteConnection = new SQLiteConnection();

                        //build db file if it does not exist
                        if (!File.Exists(filePath))
                        {
                            //create the basic file
                            SQLiteConnection.CreateFile(filePath);
                        }
                        //open the connection to the database
                        sQLiteConnection = SQLOpen(sQLiteConnection, filePath);

                        bool result = SQLCheckTableExists(String.Format(Consts.SQLiteCheckTableExists, GenericType.Name), sQLiteConnection);

                        if (result)
                        {
                            SQLExecuteList(SerializeToInsertCommands(t), sQLiteConnection);

                        }
                        else
                        {
                            SQLExecute(SerializeToTable(t), sQLiteConnection);
                            SQLExecuteList(SerializeToInsertCommands(t), sQLiteConnection);

                        }

                        SQLClose(sQLiteConnection, filePath);

                        Exists = result;

                        break;
                    default:
                        return false;
                }
            }
            else
            {
                Exists = false;
            }
            return Exists;
        }
        public bool Save<T>(List<T> t = null, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            //get the curret type
            Type GenericType = getObjectType(t);

            //get the file path
            string filePath = dataSource + "/" + GetFileExtension(dataSource, GenericType.Name, dataSourceType);

            //in order to save the file path must exist - check this.
            bool fileExists = File.Exists(filePath) ? true : false;

            //set up return variable
            bool hasSaved = false;

            switch (dataSourceType)
            {
                case DataSourceTypeEnum.XML:
                    File.WriteAllText(filePath, SerializeToXML(t));
                    hasSaved = true;
                    break;
                case DataSourceTypeEnum.CSV:
                    File.WriteAllText(filePath, SerializeToCSV(t));
                    hasSaved = true;
                    break;
                case DataSourceTypeEnum.JSON:
                    File.WriteAllText(filePath, SerializeToJSON(t));
                    hasSaved = true;
                    break;
                case DataSourceTypeEnum.SQLITE:

                    SQLiteConnection sQLiteConnection = new SQLiteConnection();

                    sQLiteConnection = SQLOpen(sQLiteConnection, filePath);

                    bool result = SQLCheckTableExists(String.Format(Consts.SQLiteCheckTableExists, GenericType.Name), sQLiteConnection);

                    if (result)
                    {
                        SQLExecuteList(SerializeToInsertCommands(t), sQLiteConnection);
                    }
                    else
                    {
                        hasSaved = false;
                    }

                    SQLClose(sQLiteConnection, filePath);

                    hasSaved = result;
                    break;

            }
            return hasSaved;
        }
        public bool Backup<T>(List<T> t = null, string dataSource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            bool isBackupDaily = false;
            bool isBackupChanges = false;
            bool hasBackedup = false;
            string LastBackup = "";
            Type GenericType = getObjectType(t);
            string filePath = Consts.DataSource + "/" + GetFileExtension(Consts.DataSource, GenericType.Name, dataSourceType);
            var backupDailyName = GetBackupFileName(DateTime.Now.ToString("yyyy.MM.dd"), Consts.BackupDirectory, Consts.BackupFileNamePrefix + GenericType.Name, GetFileSuffix(FileSuffixEnum.Daily), dataSourceType);
            var backupModificationName = GetBackupFileName(DateTime.Now.ToString("yyyy.MM.dd"), Consts.BackupDirectory, Consts.BackupFileNamePrefix + GenericType.Name, GetFileSuffix(FileSuffixEnum.Changes), dataSourceType);


            //1.ensure the backup directory exists and if not create it.
            if (!Directory.Exists(Consts.BackupDirectory))
            {
                Directory.CreateDirectory(Consts.BackupDirectory);
            }

            //get the last backup if any
            LastBackup = GetLastBackup();

            //2.ensure the datasource exists
            if (File.Exists(filePath))
            {
                //save any changes to the datasource before the backup process
                Save(t, Consts.DataSource, dataSourceType);
            }
            else
            {
                //output why we cannot backup data
                isBackupDaily = false;
                isBackupChanges = false;
                throw new FileNotFoundException("BACKUP STOPPED: File not found");
                //return hasBackedup;
            }

            //3.check to see if there is a daily backup already...
            if (File.Exists(backupDailyName))
            {

                //if the file exists then this is not a daily backup
                isBackupDaily = false;

                //check for changes made by counting objects
                var source = new FileInfo(filePath).Length;
                var backup = new FileInfo(LastBackup).Length;
                if (source > backup || backup < source)
                {
                    isBackupChanges = true;
                }
                else
                {
                    Console.WriteLine("BACKUP STOPPED: No changes made to previous JSON.");
                    isBackupChanges = false;
                    isBackupDaily = false;
                    return hasBackedup;
                }
            }
            else
            {
                isBackupChanges = false;
                isBackupDaily = true;
                Console.WriteLine("BACKUP CONTINUED: No backup made today.");
            }


            if (isBackupChanges)
            {
                //do changes to file
                File.Copy(filePath, backupModificationName);
                return true;
            }
            else if (isBackupDaily)
            {
                //do the daily backup
                File.Copy(filePath, backupDailyName);
                return true;
            }
            else
            {
                //do somthing else
                return false;
            }
        }
        #endregion

        #region Test Data

        public List<Project> SeedTestDataProjects()
        {
            int projectId = 0;
            List<Project> d = new List<Project>();
            Random rnd = new Random();
            for (int i = 0; i < 500; i++)
            {
                d.Add(new Project()
                {
                    Name = getProjectName(rnd),
                    Company = getCompanyName(rnd),
                    CreatedAt = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    ProjectId = projectId++,
                    JobIds = getRandomIds(rnd),
                    RepositoryIds = getRandomIds(rnd),
                    ServerIds = getRandomIds(rnd),
                    NoteIds = getRandomIds(rnd),
                });
            }
            return d;
        }



        public List<Job> SeedTestDataJobs()
        {
            List<Job> d = new List<Job>();
            int Id = 0;
            Random rnd = new Random();

            for (int i = 0; i < 500; i++)
            {
                d.Add(new Job()
                {
                    Id = Id++,
                    Name = getJobName(rnd),
                    JobIdentifier = getRandomNumber(rnd, 18000, 20000),
                    Progress = getJobProgress(rnd),
                    URL = getRandomURL(rnd)
                });
            }
            return d;

        }

        public List<Server> SeedTestDataServer()
        {
            //generate servers
            List<Server> d = new List<Server>();
            int Id = 0;
            Random rnd = new Random();

            for (int i = 0; i < 500; i++)
            {
                d.Add(new Server()
                {
                    Id = Id++,
                    Name = getServerName(rnd),
                    IP = getIP(rnd),
                    ServerType = getServerType(rnd),
                });
            }
            return d;
        }

        public List<Repository> SeedTestDataRepository()
        {
            int id = 0;
            Random rnd = new Random();
            List<Repository> d = new List<Repository>();
            for (int i = 0; i < 500; i++)
            {
                d.Add(new Repository()
                {
                    Id = id++,
                    Name = getRepoName(rnd),
                    RepositoryType = getRepoType(rnd),
                    URL = getRandomURL(rnd),
                    GitURL = getRandomURL(rnd),

                });
            }

            return d;
        }



        #region Seed data Generation
        private List<int> getRandomIds(Random rnd)
        {
            var selectAmount = 5;

            var Ids = new List<int>();
            var IdForReturn = new List<int>();
            int Id = 0;
            for (int i = 0; i < 500; i++)
            {
                Ids.Add(Id++);
            }

            for(int i= 0; i < selectAmount; i++)
            {
                IdForReturn.Add(Ids[rnd.Next(Ids.Count)]);
            }

            return IdForReturn.ToList();
        }
        private string getProjectName(Random rnd)
        {
            List<string> projectNames = new List<string>();
            projectNames.Add("Skiny");
            projectNames.Add("Huber");
            projectNames.Add("Hanro");
            projectNames.Add("Grainger Games");
            projectNames.Add("Game Station");
            projectNames.Add("Game");
            projectNames.Add("Starbucks");
            projectNames.Add("Costa");
            projectNames.Add("Caffe Nero");
            projectNames.Add("Barista");
            projectNames.Add("Bazar");
            projectNames.Add("Cabolacoffee");
            projectNames.Add("Falcons");
            projectNames.Add("Tradecraft");
            projectNames.Add("House of Townend");
            projectNames.Add("Gossard US");
            projectNames.Add("Gossard UK");
            projectNames.Add("Pretty Polly UK");
            projectNames.Add("Pretty Polly US");
            projectNames.Add("Chipmunks Footwear");
            projectNames.Add("Free Step");
            return projectNames[rnd.Next(projectNames.Count)];

        }
        private RepositoryTypeEnum getRepoType(Random random)
        {
            Array values = Enum.GetValues(typeof(RepositoryTypeEnum));
            return (RepositoryTypeEnum)values.GetValue(random.Next(values.Length));
        }
        private string getRepoName(Random rnd)
        {
            List<string> repoNames = new List<string>();
            repoNames.Add("Develop");
            repoNames.Add("Live");
            repoNames.Add("Staging");
            repoNames.Add("UAT");
            repoNames.Add("Development");
            repoNames.Add("Test");
            repoNames.Add("Block Box");
            repoNames.Add("White Box");
            repoNames.Add("Grey Box");
            repoNames.Add("All Box");
            return repoNames[rnd.Next(repoNames.Count)];
        }
        private string getJobName(Random rnd)
        {
            List<string> jobNames = new List<string>();
            jobNames.Add("Mix and Match Configurator");
            jobNames.Add("ASP.NET CSV uploads");
            jobNames.Add("Despatch Error");
            jobNames.Add("Video on Product Pages");
            jobNames.Add("Video on Differnt Pages");
            jobNames.Add("Redirect Issue");
            jobNames.Add("Keep box until dates");
            jobNames.Add("Reminder emails");
            jobNames.Add("Update squad profiles");
            jobNames.Add("Delete old products");
            jobNames.Add("Project kick of meeting");
            jobNames.Add("Scrum 15 minutes");
            jobNames.Add("Infinate scroll error");
            jobNames.Add("JS errror");
            jobNames.Add("CSS visual error");
            jobNames.Add("C# backend error");
            jobNames.Add("SQL database build");
            jobNames.Add("SQL Queries for application");
            jobNames.Add("NoSQL database addition");
            jobNames.Add("C# object class methods");
            jobNames.Add("Lookup table SQL");
            return jobNames[rnd.Next(jobNames.Count)];
        }
        private string getRandomURL(Random rnd)
        {

            var basePath = "www.google.com/";

            List<string> urlList = new List<string>();
            urlList.Add("cats");
            urlList.Add("dogs");
            urlList.Add("turtles");
            urlList.Add("holidays");
            urlList.Add("fly");
            urlList.Add("train");
            urlList.Add("panda");
            urlList.Add("travel");
            urlList.Add("new-york");
            urlList.Add("amazon");
            urlList.Add("asda");
            urlList.Add("tesco");
            urlList.Add("kitten");
            urlList.Add("lion");
            urlList.Add("panther");
            urlList.Add("thor");
            urlList.Add("super-man");
            return basePath + urlList[rnd.Next(urlList.Count)];
        }
        private string getRandomNumber(Random random, int min, int max)
        {
            return random.Next(min, max).ToString();
        }
        private string getServerName(Random random)
        {
            List<string> ServerNames = new List<string>();

            for (int i = 0; i < 50; i++)
            {
                ServerNames.Add($"{GetRandomCharacter(random)}{GetRandomCharacter(random)}{GetRandomCharacter(random)}.{random.Next(0, 255)}{random.Next(0, 255)}");
            }

            return ServerNames[random.Next(ServerNames.Count)];
        }
        public static char GetRandomCharacter(Random rng)
        {
            var text = "$%#@!*abcdefghijklmnopqrstuvwxyz1234567890?;:ABCDEFGHIJKLMNOPQRSTUVWXYZ^&".ToCharArray();
            int index = rng.Next(text.Length);
            return text[index];
        }
        private ServerTypesEnum getServerType(Random random)
        {
            Array values = Enum.GetValues(typeof(ServerTypesEnum));
            return (ServerTypesEnum)values.GetValue(random.Next(values.Length));
        }
        private JobProgressTypeEnum getJobProgress(Random random)
        {
            Array values = Enum.GetValues(typeof(JobProgressTypeEnum));
            return (JobProgressTypeEnum)values.GetValue(random.Next(values.Length));
        }
        private string getIP(Random random)
        {
            return $"{random.Next(1, 255)}.{random.Next(0, 255)}.{random.Next(0, 255)}.{random.Next(0, 255)}";
        }
        private string getCompanyName(Random rnd)
        {
            List<string> CompanyNames = new List<string>();
            CompanyNames.Add("McDonald's");
            CompanyNames.Add("Burger King");
            CompanyNames.Add("M&S");
            CompanyNames.Add("Tkmax");
            CompanyNames.Add("ASDA");
            CompanyNames.Add("Hard Rock Cafe");
            CompanyNames.Add("BP");
            CompanyNames.Add("Prudental");
            CompanyNames.Add("HSBC");
            CompanyNames.Add("Tesco");
            CompanyNames.Add("Aviva");
            CompanyNames.Add("Vodafone");
            CompanyNames.Add("Legal & General");
            CompanyNames.Add("Loyds");
            CompanyNames.Add("SSE");
            CompanyNames.Add("Rio Tinto");
            CompanyNames.Add("GlaxoSmithKline");
            CompanyNames.Add("Centrica");
            CompanyNames.Add("Sainsburys");
            CompanyNames.Add("Morrisons");
            CompanyNames.Add("BT");
            CompanyNames.Add("Jaguar");
            CompanyNames.Add("BAE Systems");
            CompanyNames.Add("BP");
            CompanyNames.Add("Iceland LTD");
            CompanyNames.Add("Wilkinsons");
            CompanyNames.Add("River Island");
            CompanyNames.Add("Dyson");
            CompanyNames.Add("Biffa");
            CompanyNames.Add("Poundland");
            return CompanyNames[rnd.Next(CompanyNames.Count)];

        }
        #endregion

        #endregion

        #region Import/Export Mehtods
        //Save Dialouge 
        public string getSaveFormat(DataSourceTypeEnum format)
        {
            Dictionary<string, string> saveFormats = new Dictionary<string, string>();
            //Text files (*.txt)|*.txt|All files (*.*)|*.*"'

            saveFormats.Add(DataSourceTypeEnum.JSON.ToString(), "JSON (*.json)|*.json");
            saveFormats.Add(DataSourceTypeEnum.CSV.ToString(), "Excel (*.csv)|*.csv");
            saveFormats.Add(DataSourceTypeEnum.XML.ToString(), "XML (*.xml)|*.xml");
            saveFormats.Add(DataSourceTypeEnum.SQLITE.ToString(), "SQL (*.sql)|*.sql");
            var I = saveFormats[format.ToString()];
            return I;
        }

        //serialize 
        public string SerializeTo<T>(List<T> list, DataSourceTypeEnum dataSourceTypeEnum = DataSourceTypeEnum.CSV)
        {
            string dataString = "";

            //classify the object,
            switch (dataSourceTypeEnum)
            {
                case DataSourceTypeEnum.CSV:
                    dataString = SerializeToCSV(list);
                    break;
                case DataSourceTypeEnum.JSON:
                    dataString = SerializeToJSON(list);
                    break;
                case DataSourceTypeEnum.XML:
                    dataString = SerializeToXML(list);
                    break;
                case DataSourceTypeEnum.SQLITE:
                    StringBuilder sb = new StringBuilder();
                    foreach (var s in SerializeToInsertCommands(list).ToList())
                    {
                        sb.AppendLine(s.ToString());
                    }
                    dataString = sb.ToString();
                    break;
            }

            return dataString;
        }
        public string SerializeToJSON<T>(List<T> DataList)
        {
            return JsonConvert.SerializeObject(DataList);
        }
        public string SerializeToCSV<T>(List<T> DataList)
        {

            StringBuilder csv = new StringBuilder();

            if (DataList is List<Project>)
            {
                //header names for the CSV
                csv.AppendLine(String.Join(",", typeof(Project).GetProperties().Select(x => x.Name).ToArray()));
                //write csv lines
                foreach (var p in DataList.Select((value, projectIndex) => new { value, projectIndex }))
                {
                    Project currJob = p.value as Project;
                    List<string> row = new List<string>();
                    foreach (var i in typeof(Project).GetProperties())
                    {
                        row.Add(FormatPropertyLists(p.value, i));
                    }

                    csv.AppendLine(String.Join(",", row.ToArray()));
                }
            }
            else if (DataList is List<Repository>)
            {
                //header names for the CSV
                csv.AppendLine(String.Join(",", typeof(Repository).GetProperties().Select(x => x.Name).ToArray()));

                //write csv lines
                foreach (var p in DataList.Select((value, projectIndex) => new { value, projectIndex }))
                {
                    Repository currJob = p.value as Repository;
                    List<string> row = new List<string>();
                    foreach (var i in typeof(Repository).GetProperties())
                    {
                        row.Add(Convert.ToString(i.GetValue(p.value as Repository)));
                    }

                    csv.AppendLine(String.Join(",", row.ToArray()));
                }
            }
            else if (DataList is List<Job>)
            {
                //header names for the CSV
                csv.AppendLine(String.Join(",", typeof(Job).GetProperties().Select(x => x.Name).ToArray()));

                //write csv lines
                foreach (var p in DataList.Select((value, projectIndex) => new { value, projectIndex }))
                {
                    Job currJob = p.value as Job;
                    List<string> row = new List<string>();
                    foreach (var i in typeof(Job).GetProperties())
                    {
                        row.Add(Convert.ToString(i.GetValue(p.value as Job)));
                    }

                    csv.AppendLine(String.Join(",", row.ToArray()));
                }
            }
            else if (DataList is List<Server>)
            {
                //header names for the CSV
                csv.AppendLine(String.Join(",", typeof(Server).GetProperties().Select(x => x.Name).ToArray()));

                //write csv lines
                foreach (var p in DataList.Select((value, projectIndex) => new { value, projectIndex }))
                {
                    Server currJob = p.value as Server;
                    List<string> row = new List<string>();
                    foreach (var i in typeof(Server).GetProperties())
                    {
                        row.Add(Convert.ToString(i.GetValue(p.value as Server)));
                    }

                    csv.AppendLine(String.Join(",", row.ToArray()));
                }
            }
            else if (DataList is List<Settings>)
            {
                //header names for the CSV
                csv.AppendLine(String.Join(",", typeof(Settings).GetProperties().Select(x => x.Name).ToArray()));

                //write csv lines
                foreach (var p in DataList.Select((value, projectIndex) => new { value, projectIndex }))
                {
                    Settings currJob = p.value as Settings;
                    List<string> row = new List<string>();
                    foreach (var i in typeof(Settings).GetProperties())
                    {
                        row.Add(Convert.ToString(i.GetValue(p.value as Settings)));
                    }

                    csv.AppendLine(String.Join(",", row.ToArray()));
                }
            }
            else
            {
                csv.AppendLine("datatype missing");
            }
            return csv.ToString();
        }
        public string SerializeToXML<T>(T filter)
        {

            var serializer = new XmlSerializer(typeof(T));
            string utf8;
            using (StringWriter writer = new Utf8StringWriter())
            {
                serializer.Serialize(writer, filter);
                utf8 = writer.ToString();
            }
            return utf8;
        }

        //deserialize
        public dynamic DeserializeTo(dynamic list, string data, DataSourceTypeEnum dataSourceTypeEnum = DataSourceTypeEnum.CSV)
        {

            //classify the object,
            switch (dataSourceTypeEnum)
            {
                case DataSourceTypeEnum.CSV:
                    return DeserializeFromCSV(list, data);
                case DataSourceTypeEnum.JSON:
                    return DeserializeFromJSON(list, data);
                case DataSourceTypeEnum.XML:
                    return DeserializeFromXML(list, data);
                case DataSourceTypeEnum.SQLITE:
                    break;
            }

            return null;
        }
        private dynamic DeserializeFromCSV<T>(T t, string filePath) where T : class
        {
            var CSV = from line in File.ReadAllLines(filePath).Skip(1)
                      select (line.Split(',')).ToArray();

            List<T> list = new List<T>();
            foreach (string[] line in CSV)
            {
                if (t is Server)
                {
                    Server Server = new Server();
                    Server.Id = Convert.ToInt32(line[1]);
                    Server.IP = Convert.ToString(line[2]);
                    Server.Name = Convert.ToString(line[3]);
                    Server.Notes = Convert.ToString(line[4]);
                    Server.ServerType = (ServerTypesEnum)Enum.Parse(typeof(ServerTypesEnum), Convert.ToString(line[0]));
                    list.Add(Server as T);
                }
                else if (t is Repository)
                {
                    Repository Server = new Repository();
                    Server.Id = Convert.ToInt32(line[0]);
                    Server.RepositoryType = (RepositoryTypeEnum)Enum.Parse(typeof(RepositoryTypeEnum), Convert.ToString(line[1]));
                    Server.URL = Convert.ToString(line[2]);
                    Server.GitURL = Convert.ToString(line[3]);
                    Server.Name = Convert.ToString(line[4]);
                    list.Add(Server as T);
                }
                else if (t is Job)
                {
                    Job Server = new Job();
                    Server.Id = Convert.ToInt32(line[0]);
                    Server.JobIdentifier = Convert.ToString(line[1]);
                    Server.Name = Convert.ToString(line[2]);
                    Server.URL = Convert.ToString(line[3]);
                    Server.Description = Convert.ToString(line[4]);
                    Server.AuditTrail = null;
                    Server.Progress = (JobProgressTypeEnum)Enum.Parse(typeof(JobProgressTypeEnum), Convert.ToString(line[6]));

                    list.Add(Server as T);
                }
                else if (t is Project)
                {
                    Project p = new Project();
                    p.ProjectId = Convert.ToInt32(line[0]);
                    p.Company = Convert.ToString(line[1]);
                    p.Name = Convert.ToString(line[2]);
                    p.CreatedAt = Convert.ToDateTime(line[3]);
                    p.LastUpdated = Convert.ToDateTime(line[4]);
                    p.JobIds = line[7].Split('|').Select(int.Parse).ToList();
                    p.RepositoryIds = line[5].Split('|').Select(int.Parse).ToList();
                    p.ServerIds = line[9].Split('|').Select(int.Parse).ToList();
                    p.NoteIds = line[11].Split('|').Select(int.Parse).ToList();
                    list.Add(p as T);

                }
            }
            return (list);
        }
        private dynamic DeserializeFromXML<T>(T t, string filePath) where T : class
        {
            var xdoc = File.ReadAllText(filePath);
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute("ArrayOf" + t.GetType().Name));
            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(xdoc));
            return (List<T>)serializer.Deserialize(memStream);
        }
        private dynamic DeserializeFromJSON<T>(T t, string filePath) where T : class
        {
            using (StreamReader file = File.OpenText(filePath))
            {
                return JsonConvert.DeserializeObject<List<T>>(file.ReadToEnd());
            }
        }

        //SQL
        private string SerializeToTable<T>(List<T> dataList)
        {
            StringBuilder sqlcommand = new StringBuilder();
            List<string> sqlcolumns = new List<string>();


            if (dataList is List<Project>)
            {
                foreach (var s in typeof(Project).GetProperties())
                {
                    sqlcolumns.Add(s.Name.ToString());
                }
                sqlcommand.AppendLine(String.Format(Consts.SQLiteCreate, typeof(Project).Name, String.Join(",", sqlcolumns.ToArray())));
            }
            else if (dataList is List<Repository>)
            {
                foreach (var s in typeof(Repository).GetProperties())
                {
                    sqlcolumns.Add(s.Name.ToString());
                }
                sqlcommand.AppendLine(String.Format(Consts.SQLiteCreate, typeof(Repository).Name, String.Join(",", sqlcolumns.ToArray())));
            }
            else if (dataList is List<Job>)
            {
                foreach (var s in typeof(Job).GetProperties())
                {
                    sqlcolumns.Add(s.Name.ToString());
                }
                sqlcommand.AppendLine(String.Format(Consts.SQLiteCreate, typeof(Job).Name, String.Join(",", sqlcolumns.ToArray())));
            }
            else if (dataList is List<Server>)
            {
                foreach (var s in typeof(Server).GetProperties())
                {
                    sqlcolumns.Add(s.Name.ToString());
                }
                sqlcommand.AppendLine(String.Format(Consts.SQLiteCreate, typeof(Server).Name, String.Join(",", sqlcolumns.ToArray())));
            }
            else
            {
                throw new Exception("DataType Missing!") { };
            }
            return sqlcommand.ToString();
        }
        private List<string> SerializeToInsertCommands<T>(List<T> dataList)
        {
            List<string> rows = new List<string>();
            Type obj = null;

            if (dataList is List<Project>)
            {
                obj = typeof(Project);
                foreach (var p in dataList.Select((value, index) => new { value, index }))
                {
                    List<string> property = new List<string>();
                    foreach (var i in typeof(Project).GetProperties())
                    {

                        if (i.GetValue(p.value as Project) is null)
                        {
                            property.Add("null");
                        }
                        else
                        {
                            //TODO: Make this standard: check if property is a list of int and format these as '1|2' to ensure we can parce this when loading in.
                            property.Add(FormatPropertyLists(p.value, i));
                        }

                    }
                    rows.Add(string.Format(Consts.SQLiteInsert, obj.Name, String.Join(", ", typeof(Project).GetProperties().Select(x => x.Name).ToArray().Select(x => "'" + x + "'")), String.Join(", ", property.ToArray().Select(x => "'" + x + "'"))));
                }
            }
            else if (dataList is List<Repository>)
            {
                obj = typeof(Repository);
                foreach (var p in dataList.Select((value, index) => new { value, index }))
                {
                    List<string> property = new List<string>();
                    foreach (var i in typeof(Repository).GetProperties())
                    {

                        if (i.GetValue(p.value as Repository) is null)
                        {
                            property.Add("null");
                        }
                        else
                        {

                            property.Add(Convert.ToString(i.GetValue(p.value as Repository)));
                        }

                    }

                    rows.Add(string.Format(Consts.SQLiteInsert, obj.Name, String.Join(", ", typeof(Repository).GetProperties().Select(x => x.Name).ToArray().Select(x => "'" + x + "'")), String.Join(", ", property.ToArray().Select(x => "'" + x + "'"))));
                }
            }
            else if (dataList is List<Job>)
            {
                obj = typeof(Job);
                foreach (var p in dataList.Select((value, index) => new { value, index }))
                {
                    List<string> property = new List<string>();
                    foreach (var i in typeof(Job).GetProperties())
                    {

                        if (i.GetValue(p.value as Job) is null)
                        {
                            property.Add("null");
                        }
                        else
                        {
                            property.Add(Convert.ToString(i.GetValue(p.value as Job)));
                        }

                    }
                    //https://stackoverflow.com/questions/254009/in-c-add-quotes-around-string-in-a-comma-delimited-list-of-strings
                    rows.Add(string.Format(Consts.SQLiteInsert, obj.Name, String.Join(", ", typeof(Job).GetProperties().Select(x => x.Name).ToArray().Select(x => "'" + x + "'")), String.Join(", ", property.ToArray().Select(x => "'" + x + "'"))));
                }
            }
            else if (dataList is List<Server>)
            {
                obj = typeof(Server);
                foreach (var p in dataList.Select((value, index) => new { value, index }))
                {
                    List<string> property = new List<string>();
                    foreach (var i in typeof(Server).GetProperties())
                    {

                        if (i.GetValue(p.value as Server) is null)
                        {
                            property.Add("null");
                        }
                        else
                        {
                            property.Add(Convert.ToString(i.GetValue(p.value as Server)));
                        }

                    }
                    rows.Add(string.Format(Consts.SQLiteInsert, obj.Name, String.Join(", ", typeof(Server).GetProperties().Select(x => x.Name).ToArray().Select(x => "'" + x + "'")), String.Join(", ", property.ToArray().Select(x => "'" + x + "'"))));
                }
            }

            return rows;
        }
        private dynamic DeserializeFromSQL<T>(T t, string filePath) where T : class
        {
            //check that the table exists
            SQLiteConnection sQLiteConnection = new SQLiteConnection();
            sQLiteConnection = SQLOpen(sQLiteConnection, filePath);
            bool result = SQLCheckTableExists(String.Format(Consts.SQLiteCheckTableExists, t.GetType().Name), sQLiteConnection);
            if (result)
            {
                string query = String.Format(Consts.SQLiteSelect, t.GetType().Name);
                return SQLExecuteForObjects(t, query, sQLiteConnection);
            }
            return "";
        }

        //other sql
        private SQLiteConnection SQLOpen(SQLiteConnection m_dbConnection, string dbPath)
        {
            m_dbConnection =
            new SQLiteConnection("Data Source=" + dbPath + ";Version=3;");
            m_dbConnection.Open();
            return m_dbConnection;
        }
        private void SQLClose(SQLiteConnection m_dbConnection, string dbPath)
        {
            m_dbConnection =
            new SQLiteConnection("Data Source=" + dbPath + ";Version=3;");
            m_dbConnection.Open();
        }
        private void SQLExecute(string sql, SQLiteConnection sQLiteConnection)
        {
            SQLiteCommand command = new SQLiteCommand(sql, sQLiteConnection);
            command.ExecuteReader();
        }
        private dynamic SQLExecuteForObjects<T>(T t, string sql, SQLiteConnection sQLiteConnection) where T : class
        {
            SQLiteCommand command = new SQLiteCommand(sql, sQLiteConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            List<T> objList = new List<T>();
            while (reader.Read())
            {
                if (t is Server)
                {
                    objList.Add(new Server()
                    {
                        Name = (string)reader["Name"],
                        Id = Convert.ToInt32((string)reader["id"]),
                        ServerType = (ServerTypesEnum)Enum.Parse(typeof(ServerTypesEnum), Convert.ToString((string)reader["ServerType"])),
                        IP = (string)reader["IP"],
                        Notes = (string)reader["Notes"],
                    } as T);
                }
                else if (t is List<Repository>)
                {
                    objList.Add(new Repository()
                    {
                        Name = (string)reader["Name"],
                        Id = Convert.ToInt32((string)reader["id"]),
                        RepositoryType = (RepositoryTypeEnum)Enum.Parse(typeof(RepositoryTypeEnum), Convert.ToString((string)reader["ServerType"])),
                        URL = (string)reader["URL"],
                        GitURL = (string)reader["GitURL"],
                    } as T);
                }
                else if (t is List<Job>)
                {
                    objList.Add(new Job()
                    {
                        Name = (string)reader["Name"],
                        Id = Convert.ToInt32((string)reader["id"]),
                        Progress = (JobProgressTypeEnum)Enum.Parse(typeof(JobProgressTypeEnum), Convert.ToString((string)reader["ServerType"])),
                        URL = (string)reader["URL"],
                        Description = (string)reader["Description"],
                        JobIdentifier = (string)reader["JobIdentifier"],
                    } as T);
                }
                else
                {
                    throw new Exception($"SQL Exception: table for '{t.GetType().Name}' does not exitst");
                }
            }
            return objList;
        }
        private void SQLExecuteList(List<string> CommandList, SQLiteConnection sQLiteConnection)
        {
            foreach (string command in CommandList)
            {
                SQLiteCommand sql = new SQLiteCommand(command, sQLiteConnection);
                sql.ExecuteNonQuery();
            }
        }
        private bool SQLCheckTableExists(string sql, SQLiteConnection sQLiteConnection)
        {
            bool exists = false;
            SQLiteCommand command = new SQLiteCommand(sql, sQLiteConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                exists = true;
            }
            return exists;
        }

        //xml settings
        private XmlWriterSettings GetXMLSettings()
        {
            return new XmlWriterSettings()
            {
                Indent = false,
                ConformanceLevel = ConformanceLevel.Auto,
            };
        }

        //types, mapping formatting
        public dynamic getObjectFromType(Type t)
        {
            if (t == typeof(Server)) { return new Server() { }; }
            else if (t == typeof(Repository)) { return new Repository() { }; }
            else if (t == typeof(Job)) { return new Job() { }; }
            else if (t == typeof(Project)) { return new Project() { }; }
            else if (t == typeof(User)) { return new User() { }; }
            else if (t == typeof(Email)) { return new Email() { }; }
            else if (t == typeof(Settings)) { return new Settings() { }; }
            else { throw new Exception($"No object type here for {t.Name}"); };

        }
        public Type getObjectType<T>(T dataList)
        {
            Type obj = null;
            if (dataList is List<Project> || dataList is Project) { obj = typeof(Project); }
            else if (dataList is List<Server> || dataList is Server) { obj = typeof(Server); }
            else if (dataList is List<Job> || dataList is Job) { obj = typeof(Job); }
            else if (dataList is List<Repository> || dataList is Project) { obj = typeof(Repository); }
            else if (dataList is List<Settings> || dataList is Settings) { obj = typeof(Settings); }

            else { obj = null; }
            return obj;
        }
        public object mapObjectProperites<T>(List<T> dataList)
        {
            List<T> list = new List<T>();
            foreach (var p in dataList.Select((value, projectIndex) => new { value, projectIndex }))
            {
                list.Add(p.value);
            }

            return list;

        }
        private string FormatPropertyLists(object p, PropertyInfo i)
        {
            //save a list of ints as a seperated string
            if (i.PropertyType == typeof(List<int>))
            {
                List<int> l = i.GetValue(p as Project) as List<int>;
                if (l != null)
                {
                    List<string> line = new List<string>();
                    foreach (var e in l)
                    {
                        line.Add(e.ToString());
                    }
                    return String.Join("|", line.ToArray());
                }
                return "";
            }
            //do not save the actual objects..
            else if (i.PropertyType == typeof(List<Server>) || i.PropertyType == typeof(List<Repository>) || i.PropertyType == typeof(List<Job>))
            {
                return "";
            }
            else
            {
                return Convert.ToString(i.GetValue(p as Project));

            }
        }
        #endregion

        #region File Information Methods
        public string GetFileExtension(string dataSource, string genericObject, DataSourceTypeEnum dataSourceType)
        {
            var fileSuffix = "." + dataSourceType;
            var filePrefix = genericObject + "_";
            bool hasType = (genericObject.ToString().Length > 0) ? true : false;

            if (genericObject.ToString().Length <= 0)
            {
                return "";
            }

            if (dataSource.Contains(fileSuffix))
            {
                return filePrefix + dataSource;
            }
            else
            {
                return filePrefix + dataSource + fileSuffix;
            }
        }
        public string GetLastBackup()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(Consts.BackupDirectory);

            if (directoryInfo.GetFiles().Length > 0)
            {
                var r = directoryInfo.GetFiles().OrderByDescending(f => f.LastWriteTime).First().FullName;
                if (r != null)
                {
                    return r;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }

        }
        public string GetFileSuffix(FileSuffixEnum fileSuffix = FileSuffixEnum.Daily)
        {
            string suffix = "";

            switch (fileSuffix)
            {
                case FileSuffixEnum.Daily:
                    suffix = "(Daily)";
                    break;
                case FileSuffixEnum.Changes:
                    suffix = "(Changes) " + DateTime.Now.ToString("hh-mm tt");
                    break;
            }

            return suffix;
        }
        public string GetBackupFileName(string Date, string backupDirectory = Consts.BackupDirectory, string backupNamePrefix = Consts.BackupFileNamePrefix, string backupNameSuffix = null, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            return String.Format(Consts.BackupFileNameTemplate, backupDirectory, backupNamePrefix, Date, backupNameSuffix.ToString(), dataSourceType.ToString());
        }
        #endregion

    }
}
