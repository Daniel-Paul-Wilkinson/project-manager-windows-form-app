﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.Enums;
using ProjectManager.Objects;
using ProjectManager.Interfaces;
namespace ProjectManager
{
    /// <summary>
    /// This class contains 2 interfaces using dependency injection that are used to load the data and then once it has the data interact with it.
    /// The issue was that when we where interacting directly with the datasource this application lagged.
    /// </summary>
    public class ProjectService : IProjectService
    {
        private readonly IContext _dataService;
        private readonly IProjectService _ProjectService;

        public ProjectService(IContext dataService, IProjectService projectService)
        {
            _dataService = dataService;
            _ProjectService = projectService;
        }

        //all lists that will be populated at runtime should their data...
        public List<Project> projectList = new List<Project>();
        public List<Server> serverList = new List<Server>();
        public List<Job> jobList = new List<Job>();
        public List<Repository> RepositoryList = new List<Repository>();
        public List<Email> emailList = new List<Email>();
        public List<User> userList = new List<User>();
        public List<Settings> settingsList = new List<Settings>();


        #region Project List CRUID
        public bool Save<T>(List<T> t, string datasource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            return _dataService.Save(t, datasource, dataSourceType) ? true : false;
        }

        public bool Backup<T>(List<T> t, string datasource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            return _dataService.Backup(t, datasource, dataSourceType);
        }

        public bool Create<T>(List<T> t, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            return _dataService.Create(t, dataSource, dataSourceType) ? true : false;
        }

        public dynamic Load<T>(T t, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON) where T : Type
        {
            return _dataService.Load(t, dataSource, dataSourceType);
        }

        public int GetCount<T>(T type) where T : class
        {
            if (type is Server) { return serverList.Count(); }
            else if (type is Project) { return projectList.Count(); }
            else if (type is Job) { return jobList.Count(); }
            else if (type is Repository) { return jobList.Count(); }
            else if (type is Email) { return emailList.Count(); }
            else if (type is User) { return userList.Count(); }
            else { throw new Exception($"No list for type '{type.GetType().Name}'."); }
        }


        //TODO: Write method to check main settings objects 
        public DataSourceTypeEnum GetDataSource()
        {
            var datasource = DataSourceTypeEnum.JSON;

            foreach (var setting in settingsList)
            {
                if (setting.settingType.Equals("Main"))
                {
                    datasource = (DataSourceTypeEnum)Enum.Parse(typeof(DataSourceTypeEnum), setting.datasource);
                }
            }
            return datasource;
        }

        public bool isDuplicate(dynamic obj)
        {
            bool res = false;

            if (obj is Server)
            {

                Server server = obj as Server;

                if (serverList.Exists(x => x.Id == server.Id))
                {
                    res = true;
                }
            }
            if (obj is Repository)
            {

                if (RepositoryList.Contains(obj))
                {
                    res = true;
                }
                if (obj is Job)
                {

                    if (jobList.Contains(obj))
                    {
                        res = true;
                    }
                }
                if (obj is Project)
                {

                    if (projectList.Contains(obj))
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool Populate<T>(T t, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON) where T : Type
        {
            bool hasPopulated = false;
            if (t == typeof(Server))
            {
                serverList.AddRange(_dataService.Load(t, dataSource, dataSourceType));
                hasPopulated = true;
            }
            else if (t == typeof(Project))
            {
                projectList.AddRange(_dataService.Load(t, dataSource, dataSourceType));
                hasPopulated = true;
            }
            else if (t == typeof(Job))
            {
                jobList.AddRange(_dataService.Load(t, dataSource, dataSourceType));
                hasPopulated = true;
            }
            else if (t == typeof(Repository))
            {
                RepositoryList.AddRange(_dataService.Load(t, dataSource, dataSourceType));
                hasPopulated = true;
            }
            else if (t == typeof(Email))
            {
                emailList.AddRange(_dataService.Load(t, dataSource, dataSourceType));
                hasPopulated = true;
            }
            else if (t == typeof(User))
            {
                userList.AddRange(_dataService.Load(t, dataSource, dataSourceType));
                hasPopulated = true;
            }
            else if (t == typeof(Settings))
            {
                var res = _dataService.Load(t, dataSource, dataSourceType);
                if (res != null)
                {
                    settingsList.AddRange(res);
                    hasPopulated = true;
                }
            }
            else { throw new Exception($"No list for type '{t.Name}'."); }

            return hasPopulated;
        }

        #endregion

        #region Project List Interaction Methods

        public bool AddObjects<T>(List<T> objects)
        {
            bool addedObjects = false;
            if (objects is List<Server>)
            {
                foreach (var i in objects)
                {
                    if (serverList.Contains(i as Server))
                    {
                        continue;
                    }
                    else
                    {
                        serverList.Add(i as Server);
                        addedObjects = true;
                    }

                }
            }
            else if (objects is List<Project>)
            {
                foreach (var i in objects)
                {
                    if (projectList.Contains(i as Project))
                    {
                        continue;
                    }
                    else
                    {
                        projectList.Add(i as Project);
                        addedObjects = true;
                    }

                }
            }
            else if (objects is List<Repository>)
            {
                foreach (var i in objects)
                {
                    if (RepositoryList.Contains(i as Repository))
                    {
                        continue;
                    }
                    else
                    {
                        RepositoryList.Add(i as Repository);
                        addedObjects = true;
                    }

                }
            }
            else if (objects is List<Job>)
            {
                foreach (var i in objects)
                {
                    if (jobList.Contains(i as Job))
                    {
                        continue;
                    }
                    else
                    {
                        jobList.Add(i as Job);
                        addedObjects = true;
                    }

                }
            }
            else { throw new Exception($"No object for type '{objects.GetType().Name}'."); }
            return addedObjects;
        }

        public bool RemoveObjects<T>(T Obj)
        {
            bool removed = false;

            if (Obj is List<Server>)
            {
                if (serverList.Contains(Obj as Server))
                {
                    serverList.Remove(Obj as Server);
                }
                removed = true;

            }
            else if (Obj is List<Project>)
            {
                if (serverList.Contains(Obj as Server))
                {
                    serverList.Remove(Obj as Server);
                }
                removed = true;
            }
            else if (Obj is List<Repository>)
            {
                if (serverList.Contains(Obj as Server))
                {
                    serverList.Remove(Obj as Server);
                }
                removed = true;
            }
            else if (Obj is List<Job>)
            {
                if (serverList.Contains(Obj as Server))
                {
                    serverList.Remove(Obj as Server);
                }
                removed = true;
            }
            else if (Obj is List<User>)
            {
                if (userList.Contains(Obj as User))
                {
                    userList.Remove(Obj as User);
                }
                removed = true;
            }
            else if (Obj is List<Email>)
            {
                if (emailList.Contains(Obj as Email))
                {
                    emailList.Remove(Obj as Email);
                }
            }
            else { throw new Exception($"No object for type '{Obj.GetType().Name}'."); }


            return removed;
        }

        public bool UpdateProject<T>(T oldObject, T newObject)
        {
            bool deleted = false;
            bool added = false;
            if (oldObject.GetType() == newObject.GetType())
            {
                deleted = Remove(oldObject);
                added = AddObject(newObject);
            }


            return (deleted && added) ? true : false;
        }

        private bool Remove<T>(T obj)
        {
            bool isremoved = false;
            if (!serverList.Contains(obj as Server))
            {
                serverList.Remove(obj as Server);
                isremoved = true;
            }
            else if (!projectList.Contains(obj as Project))
            {
                projectList.Remove(obj as Project);
                isremoved = true;
            }
            else if (!jobList.Contains(obj as Job))
            {
                jobList.Remove(obj as Job);
                isremoved = true;
            }
            else if (!RepositoryList.Contains(obj as Repository))
            {
                RepositoryList.Remove(obj as Repository);
                isremoved = true;
            }
            return isremoved;
        }
        private bool AddObject<T>(T newObject)
        {
            bool addedObject = false;
            if (!serverList.Contains(newObject as Server))
            {
                serverList.Add(newObject as Server);
                addedObject = true;
            }
            else if (!projectList.Contains(newObject as Project))
            {
                projectList.Add(newObject as Project);
                addedObject = true;
            }
            else if (!jobList.Contains(newObject as Job))
            {
                jobList.Add(newObject as Job);
                addedObject = true;
            }
            else if (!RepositoryList.Contains(newObject as Repository))
            {
                RepositoryList.Add(newObject as Repository);
                addedObject = true;
            }
            return addedObject;
        }

        public List<dynamic> searchFor(int sortBy, string searchterm, int projectId)
        {
            //get the results based of search term and filter type
            var filteredResult = searchOn(sortBy, searchterm, projectId);

            return filteredResult.ToList();
        }

        public IEnumerable<dynamic> searchOn(int sortBy, string searchTerm, int projectId)
        {
            IEnumerable<dynamic> dyList = null;
            List<Project> projectlist = new List<Project>();

            if (projectId > -1)
            {
                projectlist = projectList.Where(x => x.ProjectId == projectId).ToList();

                if (projectlist.Where(x => x.Name.Contains("None")).Any())
                {
                    projectlist = projectList;
                }
            }

            searchTerm = searchTerm.ToLower();

            switch (sortBy)
            {
                case 0:

                    dyList = projectList;

                    if (searchTerm.Length > 0)
                    {
                        dyList = projectlist.Select(x => x).Where(
                            x => x.Name != null && x.Name.ToLower().Contains(searchTerm) ||
                            x.LastUpdated != null && x.LastUpdated.ToString().Contains(searchTerm) ||
                            x.CreatedAt != null && x.CreatedAt.ToString().Contains(searchTerm) ||
                            x.Company != null && x.Company.ToLower().ToString().Contains(searchTerm) ||
                            x.ProjectId.ToString() != null && x.ProjectId.ToString().Contains(searchTerm)).Distinct().ToList();
                    }
                    break;
                case 1:
                    //2) get the object type
                    dyList = projectlist.Where(x => x.Servers != null).SelectMany(x => x.Servers).Distinct().ToList();

                    //if the project filter is none... 
                    if (projectlist.Where(x => x.Name.Contains("None")).Any())
                    {
                        //enfore the rule that this makes all servers appear in all lists
                        dyList = serverList;
                    }

                    //3) apply search term.
                    if (searchTerm.Length > 0)
                    {
                        dyList = dyList.Select(x => x).Where(
                             x => x.Name != null && x.Name.ToLower().Contains(searchTerm) ||
                             x.IP != null && x.IP.ToLower().Contains(searchTerm) ||
                             x.ServerType != null && x.ServerType.ToString().ToLower().Contains(searchTerm) ||
                             x.Notes != null && x.Notes.ToList().Contains(searchTerm)).Distinct().ToList();
                    }
                    break;
                case 2:
                    dyList = projectlist.Where(x => x.Jobs != null).SelectMany(h => h.Jobs).Distinct();

                    //if the project filter is none... 
                    if (projectlist.Where(x => x.Name.Contains("None")).Any())
                    {
                        //enfore the rule that this makes all servers appear in all lists
                        dyList = jobList;
                    }

                    if (searchTerm.Length > 0)
                    {
                        dyList = dyList.Select(x => x).Where(
                            x => x.Name != null && x.Name.ToLower().Contains(searchTerm) ||
                            x.Id != null && x.Id.ToString().ToLower().Contains(searchTerm) ||
                            x.JobIdentifier != null && x.JobIdentifier.ToLower().Contains(searchTerm) ||
                            x.Progress != null && x.Progress.ToString().ToLower().Contains(searchTerm) ||
                            x.Description != null && x.Description.ToLower().Contains(searchTerm)).Distinct().ToList();

                    }
                    break;
                case 3:
                    dyList = projectlist.Where(x => x.Repositories != null).SelectMany(h => h.Repositories).Distinct().ToList();

                    //if the project filter is none... 
                    if (projectlist.Where(x => x.Name.Contains("None")).Any())
                    {
                        //enfore the rule that this makes all servers appear in all lists
                        dyList = RepositoryList;
                    }

                    if (searchTerm.Length > 0)
                    {
                        dyList = dyList.Select(x => x).Where(x => x.Name != null && x.Name.ToLower().Contains(searchTerm) ||
                               x.GitURL != null && x.GitURL.ToLower().Contains(searchTerm) ||
                               x.URL != null && x.URL.ToLower().Contains(searchTerm) ||
                               x.RepositoryType != null && x.RepositoryType.ToString().Contains(searchTerm)).Distinct().ToList();
                    }
                    break;
                default:
                    dyList = projectList.Select(x => x);
                    break;
            }
            return dyList;
        }

        public bool ObjectExists<T>(T obj) where T : class
        {
            if (obj is Server) { return serverList.Contains(obj as Server) ? true : false; }
            else if (obj is Project) { return projectList.Contains(obj as Project) ? true : false; }
            else if (obj is Job) { return jobList.Contains(obj as Job) ? true : false; }
            else if (obj is Repository) { return RepositoryList.Contains(obj as Repository) ? true : false; }
            else if (obj is Email) { return emailList.Contains(obj as Email) ? true : false; }
            else if (obj is User) { return userList.Contains(obj as User) ? true : false; }
            else { throw new Exception($"No list for type '{obj.GetType().Name}'."); }
        }

        public dynamic getById<T>(T obj) where T : class
        {

            dynamic item = null;

            if (obj is Server)
            {
                var objServer = obj as Server;
                item = serverList.Find(x => x.Id == objServer.Id);
            }
            else if (obj is Project)
            {
                var objServer = obj as Project;
                item = projectList.Find(x => x.ProjectId == objServer.ProjectId);
            }
            else if (obj is Job)
            {
                var objServer = obj as Job;
                item = jobList.Find(x => x.Id == objServer.Id);
            }
            else if (obj is Repository)
            {
                var objServer = obj as Repository;
                item = RepositoryList.Find(x => x.Id == objServer.Id);
            }
            return item;
        }

        public IEnumerable<dynamic> GetCompanyReport(Type t, string company)
        {
            if (t == typeof(Project))
            {
                return from a in projectList
                       where a != null && a.Company != null && a.Company.Contains(company)
                       select a;
            }
            else
            {
                return from a in projectList
                       select a;
            }
        }

        public dynamic GetAll<T>(T t) where T : Type
        {
            if (t == typeof(Repository))
            {
                return RepositoryList;
            }
            else if (t == typeof(Job))
            {
                return jobList;
            }
            else if (t == typeof(Project))
            {
                return projectList;
            }
            else if (t == typeof(Server))
            {
                return serverList;
            }
            else if (t == typeof(Email))
            {
                return emailList;
            }
            else if (t == typeof(User))
            {
                return userList;
            }
            else if (t == typeof(Settings))
            {
                return settingsList;
            }
            else { throw new Exception($"No object for type '{t.Name}'."); }
        }

        public dynamic getProjectPropertyList<T>(List<Project> AllProjects, int index, string searchText)
        {
            List<dynamic> list = new List<dynamic>();

            switch (index)
            {
                case 0:
                    break;
                case 1:
                    list.AddRange(jobList);
                    break;
                case 2:
                    list.AddRange(RepositoryList);
                    break;
                case 3:
                    list.AddRange(serverList);
                    break;
            }
            return list;
        }

        public dynamic GetObjByFilter<T>(T t = null, string search = null) where T : Type
        {
            if (t is Server)
            {
                return serverList.Where(x => x.Name.ToLower().Contains(search.ToLower())).ToList();
            }
            else if (t is Repository)
            {
                return RepositoryList.Where(x => x.Name.ToLower().Contains(search.ToLower())).ToList();
            }
            else if (t is Job)
            {
                return jobList.Where(x => x.Name.ToLower().Contains(search.ToLower())).ToList();
            }
            else if (t is Project)
            {
                return projectList.Where(x => x.Name.ToLower().Contains(search.ToLower())).ToList();
            }
            else if (t is Email)
            {
                return emailList.Where(x => x.Personal_Email.ToLower().Contains(search.ToLower())).ToList();
            }
            else if (t is User)
            {
                return userList.Where(x => x.Forename.ToLower().Contains(search.ToLower())).ToList();
            }
            else
            {
                throw new Exception($"No such object for '{t.Name}'");
            }


        }

        public bool RemoveAll<T>(T t) where T : class
        {
            if (t is Repository)
            {
                return (RepositoryList.RemoveAll(x => x.Equals(t as Repository)) > 0) ? true : false;
            }
            else if (t is Job)
            {
                return (jobList.RemoveAll(x => x.Equals(t as Job)) > 0) ? true : false;
            }
            else if (t is Project)
            {
                return (projectList.RemoveAll(x => x.Equals(t as Project)) > 0) ? true : false;
            }
            else if (t is Server)
            {
                return (serverList.RemoveAll(x => x.Equals(t as Server)) > 0) ? true : false;
            }
            else if (t is Email)
            {
                return (emailList.RemoveAll(x => x.Equals(t as Email)) > 0) ? true : false;
            }
            else if (t is User)
            {
                return (userList.RemoveAll(x => x.Equals(t as User)) > 0) ? true : false;
            }
            else { throw new Exception($"No object for type '{t.GetType().Name}'."); }



        }

        public void SeedTestDataFor<T>(T t) where T : Type
        {
            if (t is Server)
            {
                serverList.AddRange(_dataService.SeedTestDataServer());
            }
            else if (t is Repository)
            {
                RepositoryList.AddRange(_dataService.SeedTestDataRepository());
            }
            else if (t is Job)
            {
                jobList.AddRange(_dataService.SeedTestDataJobs());
            }
            else if (t is Project)
            {
                projectList.AddRange(_dataService.SeedTestDataProjects());
            }
            else if (t is User)
            {
                //TODO : Add user test data
            }
            else if (t is Email)
            {
                //TODO : Add email test data
            }
        }

        public dynamic ReturnSeedData<T>(T t)
        {

            if (t is Server)
            {
                return _dataService.SeedTestDataServer();
            }
            else if (t is Repository)
            {
                return _dataService.SeedTestDataRepository();
            }
            else if (t is Job)
            {
                return _dataService.SeedTestDataJobs();
            }
            else if (t is Project)
            {
                return _dataService.SeedTestDataProjects();
            }
            else if (t is User)
            {
                //TODO : Add user test data
                return null;
            }
            else if (t is Email)
            {
                //TODO : Add email test data
                return null;
            }
            return null;
        }

        public int maxId()
        {
            var result = projectList.Max(x => x.ProjectId);
            return result;
        }
        #endregion

        #region Filter

        #endregion
    }
}
