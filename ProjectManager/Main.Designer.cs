﻿namespace ProjectManager
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnProjects = new System.Windows.Forms.Button();
            this.btnData = new System.Windows.Forms.Button();
            this.pSideBar = new System.Windows.Forms.Panel();
            this.btnTools = new System.Windows.Forms.Button();
            this.btnReport = new System.Windows.Forms.Button();
            this.pSettings = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pUserImage = new System.Windows.Forms.Panel();
            this.pHeader = new System.Windows.Forms.Panel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtAdvice = new System.Windows.Forms.Label();
            this.subpnlHome1 = new System.Windows.Forms.Panel();
            this.pProjects = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pHome = new System.Windows.Forms.Panel();
            this.subpnlTools = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.subpnlHome = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.subpnlReport = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.subpnlHome3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.subpnlHome2 = new System.Windows.Forms.Panel();
            this.lblData = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.cboProject = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.cboSortBy = new System.Windows.Forms.ComboBox();
            this.dgvProjectsTab = new System.Windows.Forms.DataGridView();
            this.pProject = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pData = new System.Windows.Forms.Panel();
            this.subpnlImport = new System.Windows.Forms.Panel();
            this.btnImportFinal = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.lstFileInfo = new System.Windows.Forms.ListBox();
            this.label11 = new System.Windows.Forms.Label();
            this.subpnlExport = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtExport = new System.Windows.Forms.TextBox();
            this.cboDataType = new System.Windows.Forms.ComboBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.rdoExportToFile = new System.Windows.Forms.RadioButton();
            this.rdoExportToText = new System.Windows.Forms.RadioButton();
            this.cboDataFormat = new System.Windows.Forms.ComboBox();
            this.cboDataProject = new System.Windows.Forms.ComboBox();
            this.Export = new System.Windows.Forms.Label();
            this.pnlDataSourceSettings = new System.Windows.Forms.Panel();
            this.cboDatasourceType = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.subpnlColour = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPanelSecondary = new System.Windows.Forms.RichTextBox();
            this.txtPanelPrimary = new System.Windows.Forms.RichTextBox();
            this.txtSideBarColour = new System.Windows.Forms.RichTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTitleColour = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.subpnlSettingAccount = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblImageName = new System.Windows.Forms.Label();
            this.txtAccountName = new System.Windows.Forms.RichTextBox();
            this.btnSaveAccount = new System.Windows.Forms.Button();
            this.pSetting = new System.Windows.Forms.Panel();
            this.pReports = new System.Windows.Forms.Panel();
            this.txtReportSearch = new System.Windows.Forms.TextBox();
            this.txtReportData = new System.Windows.Forms.RichTextBox();
            this.cboReportDataSource = new System.Windows.Forms.ComboBox();
            this.rdoReportExportFile = new System.Windows.Forms.RadioButton();
            this.rdoReportExportText = new System.Windows.Forms.RadioButton();
            this.btnExportReport = new System.Windows.Forms.Button();
            this.lstReport = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnFindDirectories = new System.Windows.Forms.Button();
            this.pTools = new System.Windows.Forms.Panel();
            this.subpnlImortDirectory = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.pSideBar.SuspendLayout();
            this.pHeader.SuspendLayout();
            this.subpnlHome1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pHome.SuspendLayout();
            this.subpnlTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.subpnlHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.subpnlReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.subpnlHome3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.subpnlHome2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjectsTab)).BeginInit();
            this.pProject.SuspendLayout();
            this.pData.SuspendLayout();
            this.subpnlImport.SuspendLayout();
            this.subpnlExport.SuspendLayout();
            this.pnlDataSourceSettings.SuspendLayout();
            this.subpnlColour.SuspendLayout();
            this.subpnlSettingAccount.SuspendLayout();
            this.pSetting.SuspendLayout();
            this.pReports.SuspendLayout();
            this.pTools.SuspendLayout();
            this.subpnlImortDirectory.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.Silver;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.Color.White;
            this.btnHome.Location = new System.Drawing.Point(0, 69);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(205, 43);
            this.btnHome.TabIndex = 0;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnProjects
            // 
            this.btnProjects.BackColor = System.Drawing.Color.Silver;
            this.btnProjects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProjects.ForeColor = System.Drawing.Color.White;
            this.btnProjects.Location = new System.Drawing.Point(0, 113);
            this.btnProjects.Name = "btnProjects";
            this.btnProjects.Size = new System.Drawing.Size(205, 43);
            this.btnProjects.TabIndex = 1;
            this.btnProjects.Text = "Projects";
            this.btnProjects.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProjects.UseVisualStyleBackColor = false;
            this.btnProjects.Click += new System.EventHandler(this.btnProjects_Click);
            // 
            // btnData
            // 
            this.btnData.BackColor = System.Drawing.Color.Silver;
            this.btnData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnData.ForeColor = System.Drawing.Color.White;
            this.btnData.Location = new System.Drawing.Point(1, 157);
            this.btnData.Name = "btnData";
            this.btnData.Size = new System.Drawing.Size(204, 43);
            this.btnData.TabIndex = 2;
            this.btnData.Text = "Data Management";
            this.btnData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnData.UseVisualStyleBackColor = false;
            this.btnData.Click += new System.EventHandler(this.btnData_Click);
            // 
            // pSideBar
            // 
            this.pSideBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pSideBar.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pSideBar.Controls.Add(this.btnTools);
            this.pSideBar.Controls.Add(this.btnReport);
            this.pSideBar.Controls.Add(this.pSettings);
            this.pSideBar.Controls.Add(this.btnData);
            this.pSideBar.Controls.Add(this.btnProjects);
            this.pSideBar.Controls.Add(this.btnHome);
            this.pSideBar.Location = new System.Drawing.Point(0, 0);
            this.pSideBar.Name = "pSideBar";
            this.pSideBar.Size = new System.Drawing.Size(205, 588);
            this.pSideBar.TabIndex = 0;
            this.pSideBar.Paint += new System.Windows.Forms.PaintEventHandler(this.pSideBar_Paint);
            // 
            // btnTools
            // 
            this.btnTools.BackColor = System.Drawing.Color.Silver;
            this.btnTools.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTools.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTools.ForeColor = System.Drawing.Color.White;
            this.btnTools.Location = new System.Drawing.Point(1, 245);
            this.btnTools.Name = "btnTools";
            this.btnTools.Size = new System.Drawing.Size(204, 43);
            this.btnTools.TabIndex = 15;
            this.btnTools.Text = "Tools";
            this.btnTools.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTools.UseVisualStyleBackColor = false;
            this.btnTools.Click += new System.EventHandler(this.btnTools_Click);
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.Silver;
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.ForeColor = System.Drawing.Color.White;
            this.btnReport.Location = new System.Drawing.Point(1, 201);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(204, 43);
            this.btnReport.TabIndex = 14;
            this.btnReport.Text = "Reports";
            this.btnReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReport.UseVisualStyleBackColor = false;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // pSettings
            // 
            this.pSettings.BackColor = System.Drawing.Color.Silver;
            this.pSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pSettings.ForeColor = System.Drawing.Color.White;
            this.pSettings.Location = new System.Drawing.Point(1, 289);
            this.pSettings.Name = "pSettings";
            this.pSettings.Size = new System.Drawing.Size(204, 43);
            this.pSettings.TabIndex = 13;
            this.pSettings.Text = "Settings";
            this.pSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.pSettings.UseVisualStyleBackColor = false;
            this.pSettings.Click += new System.EventHandler(this.pSettings_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Project Management -";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.White;
            this.lblUserName.Location = new System.Drawing.Point(599, 24);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(73, 25);
            this.lblUserName.TabIndex = 1;
            this.lblUserName.Text = "Daniel";
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblUserName.Click += new System.EventHandler(this.lblUserName_Click);
            // 
            // pUserImage
            // 
            this.pUserImage.Location = new System.Drawing.Point(674, 0);
            this.pUserImage.Name = "pUserImage";
            this.pUserImage.Size = new System.Drawing.Size(79, 69);
            this.pUserImage.TabIndex = 2;
            this.pUserImage.Paint += new System.Windows.Forms.PaintEventHandler(this.pUserImage_Paint);
            // 
            // pHeader
            // 
            this.pHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pHeader.BackColor = System.Drawing.Color.DimGray;
            this.pHeader.Controls.Add(this.lblHeaderTitle);
            this.pHeader.Controls.Add(this.panel1);
            this.pHeader.Controls.Add(this.pUserImage);
            this.pHeader.Controls.Add(this.lblUserName);
            this.pHeader.Controls.Add(this.label1);
            this.pHeader.Location = new System.Drawing.Point(-1, 0);
            this.pHeader.Name = "pHeader";
            this.pHeader.Size = new System.Drawing.Size(766, 69);
            this.pHeader.TabIndex = 1;
            this.pHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pHeader_Paint);
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.AutoSize = true;
            this.lblHeaderTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.ForeColor = System.Drawing.Color.White;
            this.lblHeaderTitle.Location = new System.Drawing.Point(251, 24);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Size = new System.Drawing.Size(68, 25);
            this.lblHeaderTitle.TabIndex = 14;
            this.lblHeaderTitle.Text = "Home";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(207, 68);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(949, 410);
            this.panel1.TabIndex = 12;
            // 
            // txtAdvice
            // 
            this.txtAdvice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdvice.AutoSize = true;
            this.txtAdvice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdvice.Location = new System.Drawing.Point(211, 512);
            this.txtAdvice.Name = "txtAdvice";
            this.txtAdvice.Size = new System.Drawing.Size(90, 25);
            this.txtAdvice.TabIndex = 2;
            this.txtAdvice.Text = "Advice..";
            this.txtAdvice.Click += new System.EventHandler(this.label2_Click);
            // 
            // subpnlHome1
            // 
            this.subpnlHome1.BackColor = System.Drawing.Color.DimGray;
            this.subpnlHome1.Controls.Add(this.pProjects);
            this.subpnlHome1.Controls.Add(this.pictureBox1);
            this.subpnlHome1.Location = new System.Drawing.Point(19, 15);
            this.subpnlHome1.Name = "subpnlHome1";
            this.subpnlHome1.Size = new System.Drawing.Size(143, 168);
            this.subpnlHome1.TabIndex = 6;
            this.subpnlHome1.Click += new System.EventHandler(this.subpnlHome1_Click);
            this.subpnlHome1.Paint += new System.Windows.Forms.PaintEventHandler(this.subpnlHome1_Paint);
            // 
            // pProjects
            // 
            this.pProjects.AutoSize = true;
            this.pProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pProjects.ForeColor = System.Drawing.Color.White;
            this.pProjects.Location = new System.Drawing.Point(25, 19);
            this.pProjects.Name = "pProjects";
            this.pProjects.Size = new System.Drawing.Size(90, 25);
            this.pProjects.TabIndex = 1;
            this.pProjects.Text = "Projects";
            this.pProjects.Click += new System.EventHandler(this.pProjects_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ProjectManager.Properties.Resources.NoteIcon;
            this.pictureBox1.Location = new System.Drawing.Point(25, 57);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(96, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pHome
            // 
            this.pHome.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pHome.Controls.Add(this.subpnlTools);
            this.pHome.Controls.Add(this.subpnlHome);
            this.pHome.Controls.Add(this.subpnlReport);
            this.pHome.Controls.Add(this.subpnlHome3);
            this.pHome.Controls.Add(this.subpnlHome2);
            this.pHome.Controls.Add(this.subpnlHome1);
            this.pHome.Location = new System.Drawing.Point(204, 69);
            this.pHome.Name = "pHome";
            this.pHome.Size = new System.Drawing.Size(561, 427);
            this.pHome.TabIndex = 3;
            this.pHome.Visible = false;
            this.pHome.Paint += new System.Windows.Forms.PaintEventHandler(this.pHome_Paint);
            // 
            // subpnlTools
            // 
            this.subpnlTools.BackColor = System.Drawing.Color.DimGray;
            this.subpnlTools.Controls.Add(this.pictureBox6);
            this.subpnlTools.Controls.Add(this.label10);
            this.subpnlTools.Location = new System.Drawing.Point(317, 189);
            this.subpnlTools.Name = "subpnlTools";
            this.subpnlTools.Size = new System.Drawing.Size(143, 168);
            this.subpnlTools.TabIndex = 8;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::ProjectManager.Properties.Resources.Tools;
            this.pictureBox6.Location = new System.Drawing.Point(23, 55);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(96, 89);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 3;
            this.pictureBox6.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(36, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 25);
            this.label10.TabIndex = 2;
            this.label10.Text = "Tools";
            // 
            // subpnlHome
            // 
            this.subpnlHome.BackColor = System.Drawing.Color.DimGray;
            this.subpnlHome.Controls.Add(this.pictureBox5);
            this.subpnlHome.Controls.Add(this.label6);
            this.subpnlHome.Location = new System.Drawing.Point(168, 189);
            this.subpnlHome.Name = "subpnlHome";
            this.subpnlHome.Size = new System.Drawing.Size(143, 168);
            this.subpnlHome.TabIndex = 9;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::ProjectManager.Properties.Resources._314_Address_apartment_casa_home_homepage_house_512;
            this.pictureBox5.Location = new System.Drawing.Point(25, 53);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(96, 89);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 3;
            this.pictureBox5.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(40, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 25);
            this.label6.TabIndex = 2;
            this.label6.Text = "Home";
            // 
            // subpnlReport
            // 
            this.subpnlReport.BackColor = System.Drawing.Color.DimGray;
            this.subpnlReport.Controls.Add(this.pictureBox4);
            this.subpnlReport.Controls.Add(this.label5);
            this.subpnlReport.Location = new System.Drawing.Point(19, 189);
            this.subpnlReport.Name = "subpnlReport";
            this.subpnlReport.Size = new System.Drawing.Size(143, 168);
            this.subpnlReport.TabIndex = 8;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::ProjectManager.Properties.Resources.Document_black_512;
            this.pictureBox4.Location = new System.Drawing.Point(25, 53);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(96, 89);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(29, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 25);
            this.label5.TabIndex = 2;
            this.label5.Text = "Reports";
            // 
            // subpnlHome3
            // 
            this.subpnlHome3.BackColor = System.Drawing.Color.DimGray;
            this.subpnlHome3.Controls.Add(this.label3);
            this.subpnlHome3.Controls.Add(this.pictureBox3);
            this.subpnlHome3.Location = new System.Drawing.Point(317, 16);
            this.subpnlHome3.Name = "subpnlHome3";
            this.subpnlHome3.Size = new System.Drawing.Size(143, 168);
            this.subpnlHome3.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(25, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 25);
            this.label3.TabIndex = 3;
            this.label3.Text = "Settings";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::ProjectManager.Properties.Resources._5009_200;
            this.pictureBox3.Location = new System.Drawing.Point(23, 56);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(88, 88);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // subpnlHome2
            // 
            this.subpnlHome2.BackColor = System.Drawing.Color.DimGray;
            this.subpnlHome2.Controls.Add(this.lblData);
            this.subpnlHome2.Controls.Add(this.pictureBox2);
            this.subpnlHome2.Location = new System.Drawing.Point(168, 15);
            this.subpnlHome2.Name = "subpnlHome2";
            this.subpnlHome2.Size = new System.Drawing.Size(143, 168);
            this.subpnlHome2.TabIndex = 7;
            this.subpnlHome2.Click += new System.EventHandler(this.subpnlHome2_Click);
            this.subpnlHome2.Paint += new System.Windows.Forms.PaintEventHandler(this.subpnlHome2_Paint);
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.ForeColor = System.Drawing.Color.White;
            this.lblData.Location = new System.Drawing.Point(46, 19);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(57, 25);
            this.lblData.TabIndex = 2;
            this.lblData.Text = "Data";
            this.lblData.Click += new System.EventHandler(this.lblData_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ProjectManager.Properties.Resources.dataicon;
            this.pictureBox2.Location = new System.Drawing.Point(30, 57);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(91, 90);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click_1);
            // 
            // cboProject
            // 
            this.cboProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProject.FormattingEnabled = true;
            this.cboProject.Location = new System.Drawing.Point(19, 88);
            this.cboProject.Name = "cboProject";
            this.cboProject.Size = new System.Drawing.Size(511, 33);
            this.cboProject.TabIndex = 3;
            this.cboProject.SelectedIndexChanged += new System.EventHandler(this.cboProject_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(21, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Sort By";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(21, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Project";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(19, 29);
            this.txtSearch.Multiline = true;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(511, 30);
            this.txtSearch.TabIndex = 12;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // cboSortBy
            // 
            this.cboSortBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSortBy.FormattingEnabled = true;
            this.cboSortBy.Location = new System.Drawing.Point(19, 150);
            this.cboSortBy.Name = "cboSortBy";
            this.cboSortBy.Size = new System.Drawing.Size(511, 33);
            this.cboSortBy.TabIndex = 1;
            this.cboSortBy.SelectedIndexChanged += new System.EventHandler(this.cboSortBy_SelectedIndexChanged);
            // 
            // dgvProjectsTab
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProjectsTab.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProjectsTab.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProjectsTab.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvProjectsTab.Location = new System.Drawing.Point(18, 200);
            this.dgvProjectsTab.Name = "dgvProjectsTab";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProjectsTab.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvProjectsTab.RowTemplate.Height = 24;
            this.dgvProjectsTab.Size = new System.Drawing.Size(512, 213);
            this.dgvProjectsTab.TabIndex = 0;
            this.dgvProjectsTab.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dgvProjectsTab.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProjectsTab_CellValueChanged);
            // 
            // pProject
            // 
            this.pProject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pProject.Controls.Add(this.label8);
            this.pProject.Controls.Add(this.dgvProjectsTab);
            this.pProject.Controls.Add(this.cboSortBy);
            this.pProject.Controls.Add(this.txtSearch);
            this.pProject.Controls.Add(this.label9);
            this.pProject.Controls.Add(this.label7);
            this.pProject.Controls.Add(this.cboProject);
            this.pProject.Location = new System.Drawing.Point(204, 69);
            this.pProject.Name = "pProject";
            this.pProject.Size = new System.Drawing.Size(558, 427);
            this.pProject.TabIndex = 9;
            this.pProject.Paint += new System.Windows.Forms.PaintEventHandler(this.pProject_Paint);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "Search";
            // 
            // pData
            // 
            this.pData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pData.BackColor = System.Drawing.Color.Transparent;
            this.pData.Controls.Add(this.subpnlImport);
            this.pData.Controls.Add(this.subpnlExport);
            this.pData.Location = new System.Drawing.Point(204, 69);
            this.pData.Name = "pData";
            this.pData.Size = new System.Drawing.Size(558, 427);
            this.pData.TabIndex = 12;
            this.pData.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // subpnlImport
            // 
            this.subpnlImport.Controls.Add(this.btnImportFinal);
            this.subpnlImport.Controls.Add(this.btnImport);
            this.subpnlImport.Controls.Add(this.lstFileInfo);
            this.subpnlImport.Controls.Add(this.label11);
            this.subpnlImport.Location = new System.Drawing.Point(274, 19);
            this.subpnlImport.Name = "subpnlImport";
            this.subpnlImport.Size = new System.Drawing.Size(254, 394);
            this.subpnlImport.TabIndex = 1;
            // 
            // btnImportFinal
            // 
            this.btnImportFinal.Location = new System.Drawing.Point(15, 347);
            this.btnImportFinal.Name = "btnImportFinal";
            this.btnImportFinal.Size = new System.Drawing.Size(220, 31);
            this.btnImportFinal.TabIndex = 10;
            this.btnImportFinal.Text = "Import";
            this.btnImportFinal.UseVisualStyleBackColor = true;
            this.btnImportFinal.Click += new System.EventHandler(this.btnImportFinal_Click);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(15, 314);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(220, 27);
            this.btnImport.TabIndex = 9;
            this.btnImport.Text = "Select File and Review";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // lstFileInfo
            // 
            this.lstFileInfo.FormattingEnabled = true;
            this.lstFileInfo.ItemHeight = 16;
            this.lstFileInfo.Location = new System.Drawing.Point(15, 39);
            this.lstFileInfo.Name = "lstFileInfo";
            this.lstFileInfo.Size = new System.Drawing.Size(220, 260);
            this.lstFileInfo.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 17);
            this.label11.TabIndex = 1;
            this.label11.Text = "Import Data";
            // 
            // subpnlExport
            // 
            this.subpnlExport.Controls.Add(this.label14);
            this.subpnlExport.Controls.Add(this.label13);
            this.subpnlExport.Controls.Add(this.label12);
            this.subpnlExport.Controls.Add(this.txtExport);
            this.subpnlExport.Controls.Add(this.cboDataType);
            this.subpnlExport.Controls.Add(this.btnExport);
            this.subpnlExport.Controls.Add(this.rdoExportToFile);
            this.subpnlExport.Controls.Add(this.rdoExportToText);
            this.subpnlExport.Controls.Add(this.cboDataFormat);
            this.subpnlExport.Controls.Add(this.cboDataProject);
            this.subpnlExport.Controls.Add(this.Export);
            this.subpnlExport.Location = new System.Drawing.Point(19, 17);
            this.subpnlExport.Name = "subpnlExport";
            this.subpnlExport.Size = new System.Drawing.Size(245, 396);
            this.subpnlExport.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 133);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 17);
            this.label14.TabIndex = 11;
            this.label14.Text = "Project related:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 82);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 17);
            this.label13.TabIndex = 10;
            this.label13.Text = "Export Format";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 35);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 17);
            this.label12.TabIndex = 9;
            this.label12.Text = "Projects";
            // 
            // txtExport
            // 
            this.txtExport.Location = new System.Drawing.Point(13, 240);
            this.txtExport.Multiline = true;
            this.txtExport.Name = "txtExport";
            this.txtExport.Size = new System.Drawing.Size(216, 101);
            this.txtExport.TabIndex = 8;
            // 
            // cboDataType
            // 
            this.cboDataType.FormattingEnabled = true;
            this.cboDataType.Location = new System.Drawing.Point(14, 153);
            this.cboDataType.Name = "cboDataType";
            this.cboDataType.Size = new System.Drawing.Size(215, 24);
            this.cboDataType.TabIndex = 7;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(14, 347);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(215, 33);
            this.btnExport.TabIndex = 7;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // rdoExportToFile
            // 
            this.rdoExportToFile.AutoSize = true;
            this.rdoExportToFile.Location = new System.Drawing.Point(14, 210);
            this.rdoExportToFile.Name = "rdoExportToFile";
            this.rdoExportToFile.Size = new System.Drawing.Size(116, 21);
            this.rdoExportToFile.TabIndex = 6;
            this.rdoExportToFile.TabStop = true;
            this.rdoExportToFile.Text = "Export To File";
            this.rdoExportToFile.UseVisualStyleBackColor = true;
            // 
            // rdoExportToText
            // 
            this.rdoExportToText.AutoSize = true;
            this.rdoExportToText.Location = new System.Drawing.Point(13, 183);
            this.rdoExportToText.Name = "rdoExportToText";
            this.rdoExportToText.Size = new System.Drawing.Size(121, 21);
            this.rdoExportToText.TabIndex = 5;
            this.rdoExportToText.TabStop = true;
            this.rdoExportToText.Text = "Export To Text";
            this.rdoExportToText.UseVisualStyleBackColor = true;
            // 
            // cboDataFormat
            // 
            this.cboDataFormat.FormattingEnabled = true;
            this.cboDataFormat.Location = new System.Drawing.Point(13, 102);
            this.cboDataFormat.Name = "cboDataFormat";
            this.cboDataFormat.Size = new System.Drawing.Size(216, 24);
            this.cboDataFormat.TabIndex = 3;
            // 
            // cboDataProject
            // 
            this.cboDataProject.FormattingEnabled = true;
            this.cboDataProject.Location = new System.Drawing.Point(14, 55);
            this.cboDataProject.Name = "cboDataProject";
            this.cboDataProject.Size = new System.Drawing.Size(215, 24);
            this.cboDataProject.TabIndex = 2;
            // 
            // Export
            // 
            this.Export.AutoSize = true;
            this.Export.Location = new System.Drawing.Point(10, 8);
            this.Export.Name = "Export";
            this.Export.Size = new System.Drawing.Size(82, 17);
            this.Export.TabIndex = 0;
            this.Export.Text = "Export Data";
            // 
            // pnlDataSourceSettings
            // 
            this.pnlDataSourceSettings.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlDataSourceSettings.Controls.Add(this.cboDatasourceType);
            this.pnlDataSourceSettings.Controls.Add(this.label22);
            this.pnlDataSourceSettings.Location = new System.Drawing.Point(17, 98);
            this.pnlDataSourceSettings.Name = "pnlDataSourceSettings";
            this.pnlDataSourceSettings.Size = new System.Drawing.Size(288, 35);
            this.pnlDataSourceSettings.TabIndex = 11;
            // 
            // cboDatasourceType
            // 
            this.cboDatasourceType.FormattingEnabled = true;
            this.cboDatasourceType.Location = new System.Drawing.Point(135, 4);
            this.cboDatasourceType.Name = "cboDatasourceType";
            this.cboDatasourceType.Size = new System.Drawing.Size(146, 24);
            this.cboDatasourceType.TabIndex = 7;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(117, 17);
            this.label22.TabIndex = 4;
            this.label22.Text = "Datasource Type";
            // 
            // subpnlColour
            // 
            this.subpnlColour.BackColor = System.Drawing.SystemColors.ControlDark;
            this.subpnlColour.Controls.Add(this.label20);
            this.subpnlColour.Controls.Add(this.label16);
            this.subpnlColour.Controls.Add(this.txtPanelSecondary);
            this.subpnlColour.Controls.Add(this.txtPanelPrimary);
            this.subpnlColour.Controls.Add(this.txtSideBarColour);
            this.subpnlColour.Controls.Add(this.label17);
            this.subpnlColour.Controls.Add(this.label18);
            this.subpnlColour.Controls.Add(this.txtTitleColour);
            this.subpnlColour.Controls.Add(this.button3);
            this.subpnlColour.Location = new System.Drawing.Point(17, 135);
            this.subpnlColour.Name = "subpnlColour";
            this.subpnlColour.Size = new System.Drawing.Size(289, 198);
            this.subpnlColour.TabIndex = 11;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 118);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(101, 17);
            this.label20.TabIndex = 11;
            this.label20.Text = "Panel Colour 2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 84);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(101, 17);
            this.label16.TabIndex = 10;
            this.label16.Text = "Panel Colour 1";
            // 
            // txtPanelSecondary
            // 
            this.txtPanelSecondary.Location = new System.Drawing.Point(133, 115);
            this.txtPanelSecondary.Name = "txtPanelSecondary";
            this.txtPanelSecondary.Size = new System.Drawing.Size(147, 25);
            this.txtPanelSecondary.TabIndex = 9;
            this.txtPanelSecondary.Text = "";
            // 
            // txtPanelPrimary
            // 
            this.txtPanelPrimary.Location = new System.Drawing.Point(133, 80);
            this.txtPanelPrimary.Name = "txtPanelPrimary";
            this.txtPanelPrimary.Size = new System.Drawing.Size(147, 25);
            this.txtPanelPrimary.TabIndex = 8;
            this.txtPanelPrimary.Text = "";
            // 
            // txtSideBarColour
            // 
            this.txtSideBarColour.Location = new System.Drawing.Point(134, 45);
            this.txtSideBarColour.Name = "txtSideBarColour";
            this.txtSideBarColour.Size = new System.Drawing.Size(147, 25);
            this.txtSideBarColour.TabIndex = 6;
            this.txtSideBarColour.Text = "";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 17);
            this.label17.TabIndex = 4;
            this.label17.Text = "Title Colour";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 50);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 17);
            this.label18.TabIndex = 2;
            this.label18.Text = "Sidebar Colour";
            // 
            // txtTitleColour
            // 
            this.txtTitleColour.Location = new System.Drawing.Point(134, 10);
            this.txtTitleColour.Name = "txtTitleColour";
            this.txtTitleColour.Size = new System.Drawing.Size(147, 25);
            this.txtTitleColour.TabIndex = 3;
            this.txtTitleColour.Text = "";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(133, 148);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(147, 42);
            this.button3.TabIndex = 5;
            this.button3.Text = "Check Colours";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.btnTestColours_Click);
            // 
            // subpnlSettingAccount
            // 
            this.subpnlSettingAccount.BackColor = System.Drawing.SystemColors.ControlDark;
            this.subpnlSettingAccount.Controls.Add(this.label2);
            this.subpnlSettingAccount.Controls.Add(this.button1);
            this.subpnlSettingAccount.Controls.Add(this.lblImageName);
            this.subpnlSettingAccount.Controls.Add(this.txtAccountName);
            this.subpnlSettingAccount.Location = new System.Drawing.Point(17, 15);
            this.subpnlSettingAccount.Name = "subpnlSettingAccount";
            this.subpnlSettingAccount.Size = new System.Drawing.Size(289, 81);
            this.subpnlSettingAccount.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Account Name";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(133, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(147, 31);
            this.button1.TabIndex = 0;
            this.button1.Text = "Select Account Image";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.SelectAccountImage_Click);
            // 
            // lblImageName
            // 
            this.lblImageName.AutoSize = true;
            this.lblImageName.Location = new System.Drawing.Point(11, 48);
            this.lblImageName.Name = "lblImageName";
            this.lblImageName.Size = new System.Drawing.Size(101, 17);
            this.lblImageName.TabIndex = 2;
            this.lblImageName.Text = "Account Image";
            // 
            // txtAccountName
            // 
            this.txtAccountName.Location = new System.Drawing.Point(133, 10);
            this.txtAccountName.Name = "txtAccountName";
            this.txtAccountName.Size = new System.Drawing.Size(148, 25);
            this.txtAccountName.TabIndex = 3;
            this.txtAccountName.Text = "";
            // 
            // btnSaveAccount
            // 
            this.btnSaveAccount.BackColor = System.Drawing.Color.Green;
            this.btnSaveAccount.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSaveAccount.Location = new System.Drawing.Point(17, 337);
            this.btnSaveAccount.Name = "btnSaveAccount";
            this.btnSaveAccount.Size = new System.Drawing.Size(289, 42);
            this.btnSaveAccount.TabIndex = 5;
            this.btnSaveAccount.Text = "Save";
            this.btnSaveAccount.UseVisualStyleBackColor = false;
            this.btnSaveAccount.Click += new System.EventHandler(this.btnSaveAccount_Click);
            // 
            // pSetting
            // 
            this.pSetting.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pSetting.Controls.Add(this.pnlDataSourceSettings);
            this.pSetting.Controls.Add(this.subpnlColour);
            this.pSetting.Controls.Add(this.subpnlSettingAccount);
            this.pSetting.Controls.Add(this.btnSaveAccount);
            this.pSetting.Location = new System.Drawing.Point(205, 69);
            this.pSetting.Name = "pSetting";
            this.pSetting.Size = new System.Drawing.Size(558, 427);
            this.pSetting.TabIndex = 1;
            this.pSetting.Paint += new System.Windows.Forms.PaintEventHandler(this.pSettings_Paint);
            // 
            // pReports
            // 
            this.pReports.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pReports.Controls.Add(this.txtReportSearch);
            this.pReports.Controls.Add(this.txtReportData);
            this.pReports.Controls.Add(this.cboReportDataSource);
            this.pReports.Controls.Add(this.rdoReportExportFile);
            this.pReports.Controls.Add(this.rdoReportExportText);
            this.pReports.Controls.Add(this.btnExportReport);
            this.pReports.Controls.Add(this.lstReport);
            this.pReports.Location = new System.Drawing.Point(204, 70);
            this.pReports.Name = "pReports";
            this.pReports.Size = new System.Drawing.Size(561, 426);
            this.pReports.TabIndex = 13;
            // 
            // txtReportSearch
            // 
            this.txtReportSearch.Location = new System.Drawing.Point(14, 18);
            this.txtReportSearch.Name = "txtReportSearch";
            this.txtReportSearch.Size = new System.Drawing.Size(189, 22);
            this.txtReportSearch.TabIndex = 6;
            this.txtReportSearch.TextChanged += new System.EventHandler(this.txtReportSearch_TextChanged);
            // 
            // txtReportData
            // 
            this.txtReportData.Location = new System.Drawing.Point(209, 17);
            this.txtReportData.Name = "txtReportData";
            this.txtReportData.Size = new System.Drawing.Size(321, 388);
            this.txtReportData.TabIndex = 5;
            this.txtReportData.Text = "";
            // 
            // cboReportDataSource
            // 
            this.cboReportDataSource.FormattingEnabled = true;
            this.cboReportDataSource.Location = new System.Drawing.Point(16, 347);
            this.cboReportDataSource.Name = "cboReportDataSource";
            this.cboReportDataSource.Size = new System.Drawing.Size(188, 24);
            this.cboReportDataSource.TabIndex = 4;
            // 
            // rdoReportExportFile
            // 
            this.rdoReportExportFile.AutoSize = true;
            this.rdoReportExportFile.Location = new System.Drawing.Point(17, 322);
            this.rdoReportExportFile.Name = "rdoReportExportFile";
            this.rdoReportExportFile.Size = new System.Drawing.Size(116, 21);
            this.rdoReportExportFile.TabIndex = 3;
            this.rdoReportExportFile.TabStop = true;
            this.rdoReportExportFile.Text = "Export To File";
            this.rdoReportExportFile.UseVisualStyleBackColor = true;
            // 
            // rdoReportExportText
            // 
            this.rdoReportExportText.AutoSize = true;
            this.rdoReportExportText.Location = new System.Drawing.Point(16, 299);
            this.rdoReportExportText.Name = "rdoReportExportText";
            this.rdoReportExportText.Size = new System.Drawing.Size(143, 21);
            this.rdoReportExportText.TabIndex = 2;
            this.rdoReportExportText.TabStop = true;
            this.rdoReportExportText.Text = "Export To Textbox";
            this.rdoReportExportText.UseVisualStyleBackColor = true;
            this.rdoReportExportText.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // btnExportReport
            // 
            this.btnExportReport.Location = new System.Drawing.Point(14, 377);
            this.btnExportReport.Name = "btnExportReport";
            this.btnExportReport.Size = new System.Drawing.Size(190, 29);
            this.btnExportReport.TabIndex = 1;
            this.btnExportReport.Text = "Export Report";
            this.btnExportReport.UseVisualStyleBackColor = true;
            this.btnExportReport.Click += new System.EventHandler(this.btnExportReport_Click);
            // 
            // lstReport
            // 
            this.lstReport.FormattingEnabled = true;
            this.lstReport.ItemHeight = 16;
            this.lstReport.Location = new System.Drawing.Point(14, 53);
            this.lstReport.Name = "lstReport";
            this.lstReport.Size = new System.Drawing.Size(189, 244);
            this.lstReport.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Add Project Directory To System";
            // 
            // btnFindDirectories
            // 
            this.btnFindDirectories.Location = new System.Drawing.Point(189, 146);
            this.btnFindDirectories.Name = "btnFindDirectories";
            this.btnFindDirectories.Size = new System.Drawing.Size(101, 31);
            this.btnFindDirectories.TabIndex = 1;
            this.btnFindDirectories.Text = "Select";
            this.btnFindDirectories.UseVisualStyleBackColor = true;
            this.btnFindDirectories.Click += new System.EventHandler(this.btnFindDirectories_Click);
            // 
            // pTools
            // 
            this.pTools.Controls.Add(this.subpnlImortDirectory);
            this.pTools.Location = new System.Drawing.Point(206, 68);
            this.pTools.Name = "pTools";
            this.pTools.Size = new System.Drawing.Size(560, 428);
            this.pTools.TabIndex = 14;
            this.pTools.Visible = false;
            // 
            // subpnlImortDirectory
            // 
            this.subpnlImortDirectory.Controls.Add(this.listBox1);
            this.subpnlImortDirectory.Controls.Add(this.btnFindDirectories);
            this.subpnlImortDirectory.Controls.Add(this.label4);
            this.subpnlImortDirectory.Location = new System.Drawing.Point(10, 8);
            this.subpnlImortDirectory.Name = "subpnlImortDirectory";
            this.subpnlImortDirectory.Size = new System.Drawing.Size(296, 187);
            this.subpnlImortDirectory.TabIndex = 2;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(8, 27);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(282, 116);
            this.listBox1.TabIndex = 2;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 587);
            this.ControlBox = false;
            this.Controls.Add(this.txtAdvice);
            this.Controls.Add(this.pHeader);
            this.Controls.Add(this.pSideBar);
            this.Controls.Add(this.pHome);
            this.Controls.Add(this.pData);
            this.Controls.Add(this.pReports);
            this.Controls.Add(this.pTools);
            this.Controls.Add(this.pSetting);
            this.Controls.Add(this.pProject);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pSideBar.ResumeLayout(false);
            this.pHeader.ResumeLayout(false);
            this.pHeader.PerformLayout();
            this.subpnlHome1.ResumeLayout(false);
            this.subpnlHome1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pHome.ResumeLayout(false);
            this.subpnlTools.ResumeLayout(false);
            this.subpnlTools.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.subpnlHome.ResumeLayout(false);
            this.subpnlHome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.subpnlReport.ResumeLayout(false);
            this.subpnlReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.subpnlHome3.ResumeLayout(false);
            this.subpnlHome3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.subpnlHome2.ResumeLayout(false);
            this.subpnlHome2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjectsTab)).EndInit();
            this.pProject.ResumeLayout(false);
            this.pProject.PerformLayout();
            this.pData.ResumeLayout(false);
            this.subpnlImport.ResumeLayout(false);
            this.subpnlImport.PerformLayout();
            this.subpnlExport.ResumeLayout(false);
            this.subpnlExport.PerformLayout();
            this.pnlDataSourceSettings.ResumeLayout(false);
            this.pnlDataSourceSettings.PerformLayout();
            this.subpnlColour.ResumeLayout(false);
            this.subpnlColour.PerformLayout();
            this.subpnlSettingAccount.ResumeLayout(false);
            this.subpnlSettingAccount.PerformLayout();
            this.pSetting.ResumeLayout(false);
            this.pReports.ResumeLayout(false);
            this.pReports.PerformLayout();
            this.pTools.ResumeLayout(false);
            this.subpnlImortDirectory.ResumeLayout(false);
            this.subpnlImortDirectory.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnProjects;
        private System.Windows.Forms.Button btnData;
        private System.Windows.Forms.Panel pSideBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Panel pUserImage;
        private System.Windows.Forms.Panel pHeader;
        private System.Windows.Forms.Label txtAdvice;
        private System.Windows.Forms.Panel subpnlHome1;
        private System.Windows.Forms.Label pProjects;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pHome;
        private System.Windows.Forms.ComboBox cboProject;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.ComboBox cboSortBy;
        private System.Windows.Forms.DataGridView dgvProjectsTab;
        private System.Windows.Forms.Panel pProject;
        private System.Windows.Forms.Panel pData;
        private System.Windows.Forms.Panel subpnlExport;
        private System.Windows.Forms.ComboBox cboDataFormat;
        private System.Windows.Forms.ComboBox cboDataProject;
        private System.Windows.Forms.Label Export;
        private System.Windows.Forms.RadioButton rdoExportToFile;
        private System.Windows.Forms.RadioButton rdoExportToText;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Panel subpnlImport;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.ListBox lstFileInfo;
        private System.Windows.Forms.ComboBox cboDataType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtExport;
        private System.Windows.Forms.Button btnImportFinal;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button pSettings;
        private System.Windows.Forms.Label lblImageName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnSaveAccount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox txtAccountName;
        private System.Windows.Forms.Panel subpnlColour;
        private System.Windows.Forms.RichTextBox txtSideBarColour;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RichTextBox txtTitleColour;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel subpnlSettingAccount;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RichTextBox txtPanelSecondary;
        private System.Windows.Forms.RichTextBox txtPanelPrimary;
        private System.Windows.Forms.Panel pnlDataSourceSettings;
        private System.Windows.Forms.ComboBox cboDatasourceType;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnTools;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Panel subpnlHome3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel subpnlHome2;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pSetting;
        private System.Windows.Forms.Panel pReports;
        private System.Windows.Forms.TextBox txtReportSearch;
        private System.Windows.Forms.RichTextBox txtReportData;
        private System.Windows.Forms.ComboBox cboReportDataSource;
        private System.Windows.Forms.RadioButton rdoReportExportFile;
        private System.Windows.Forms.RadioButton rdoReportExportText;
        private System.Windows.Forms.Button btnExportReport;
        private System.Windows.Forms.ListBox lstReport;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFindDirectories;
        private System.Windows.Forms.Panel pTools;
        private System.Windows.Forms.Panel subpnlImortDirectory;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Panel subpnlTools;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel subpnlHome;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel subpnlReport;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
    }
}

