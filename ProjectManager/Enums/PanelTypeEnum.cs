﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Enums
{
    public enum PanelTypeEnum
    {
        [Description("SubPanel")]
        SubPanel,
        [Description("MainPanel")]
        MainPanel,
        [Description("SidePanel")]
        SidePanel,
        [Description("HeaderPanel")]
        HeaderPanel,
    }
}
