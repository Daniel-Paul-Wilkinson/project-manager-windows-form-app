﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Enums
{
    public enum ServerTypesEnum
    {
        Database = 0,
        Web = 2,
        Both = 3,
    }
}
