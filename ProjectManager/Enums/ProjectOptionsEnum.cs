﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Enums
{
    public enum ProjectOptionsEnum
    {
        [Description("Jobs")]
        Job,
        [Description("Server")]
        Server,
        [Description("Repositories")]
        Repository,
        [Description("Notes")]
        Note,
        [Description("Projects")]
        Project,
    }
}
