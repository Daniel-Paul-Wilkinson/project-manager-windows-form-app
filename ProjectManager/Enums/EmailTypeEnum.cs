﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Enums
{
    public enum EmailTypeEnum
    {
        [Description("Developer Link")]
        DeveloperLink = 1,
        [Description("Project Information")]
        ProjectInformation = 2,
        [Description("Server Information")]
        ServerInformation = 3,
        [Description("Job Information")]
        JobInformation = 4,

    }
}
