﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Enums
{
    public enum FileSuffixEnum
    {
        [Description("Daily")]
        Daily,
        [Description("Changes")]
        Changes,
    }
}
