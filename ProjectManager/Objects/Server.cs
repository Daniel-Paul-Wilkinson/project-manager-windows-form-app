﻿using ProjectManager.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ProjectManager.Objects
{
    //A project contains many servers
    [Serializable]
    public class Server
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IP { get; set; }
        public ServerTypesEnum ServerType { get; set; }
        public string Notes { get; set; }
        public Server()
        {
        }

       

        public override string ToString()
        {
            return ServerType + " - " + Name;
        }
    }
}
