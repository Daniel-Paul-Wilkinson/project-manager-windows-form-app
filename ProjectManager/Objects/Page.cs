﻿using ProjectManager.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectManager.Objects
{
    public class Page
    {
        public string name { get; set; }
        public string formTitle { get; set; }
        public string panelTitle { get; set; }
        public Panel panel { get; set; }
        public PanelTypeEnum PanelType { get; set; }
        public bool ActiveAlways { get; set; }

        Page() { }

        public Page(string name, string formTitle, string panelTitle, Panel panel, PanelTypeEnum panelType, bool ActiveAlways)
        {
            this.name = name;
            this.formTitle = formTitle;
            this.panelTitle = panelTitle;
            this.panel = panel;
            this.ActiveAlways = ActiveAlways;
            PanelType = panelType;
        }
    }
}
