﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ProjectManager.Objects;

namespace ProjectManager.Objects
{
    //Main class in system
    [Serializable]
    public class Project
    {
        public int ProjectId { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastUpdated { get; set; }

        //Repos
        public List<int> RepositoryIds{ get; set; }
        public List<Repository> Repositories { get; set; }

        //Jobs
        public List<int> JobIds { get; set; }
        public List<Job> Jobs { get; set; }

        //Servers
        public List<int> ServerIds { get; set; }
        public List<Server> Servers { get; set; }

        //Notes
        public List<int> NoteIds { get; set; }
        public List<string> Notes { get; set; }
        
        
        public Project()
        {

        }


        public override string ToString()
        {
            return Name;
        }

    }
}
