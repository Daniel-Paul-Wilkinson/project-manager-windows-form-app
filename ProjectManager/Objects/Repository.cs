﻿using ProjectManager.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ProjectManager.Objects
{
    //A project contains many repositories
    [Serializable]
    public class Repository
    {
        public int Id { get; set; }
        public RepositoryTypeEnum RepositoryType { get; set; }
        public string URL { get; set; }
        public string GitURL { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return RepositoryType + " - " + Name;
        }

    }
}
