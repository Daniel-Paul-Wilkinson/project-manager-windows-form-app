﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Objects
{
    public class Logger
    {
        public string time = DateTime.Now.ToString();
        public string message { get; set; }

        public Logger(string message)
        {
            this.message = message;
        }

        public Logger()
        {
        }

    }
}
