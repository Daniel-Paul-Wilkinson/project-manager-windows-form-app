﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager
{

    public enum JobRole
    {
        Developer,
        SEO,
        AccountManager,

    }
   public class User
    {
        public string Id{ get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Number { get; set; }
        public JobRole jobRole { get; set; }
        public User()
        {

        }
        public override string ToString()
        {
            return Forename +" "+ Surname;
        }

    }
    class UserList
    {
        public static List<User> userList = new List<User>();

        public static void AddUser(List<User> users)
        {
            userList.AddRange(users);
        }
        public static List<User> GetAll()
        {
            return userList.ToList();
        }

    }
}
