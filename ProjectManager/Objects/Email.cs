﻿using ProjectManager.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ProjectManager.Objects
{
    public class Email
    {
        public string id { get; set; }
        public string FromForename { get; set; }
        public string FromSurname { get; set; }
        public string Subject { get; set; }
        public bool isHTML { get; set; }
        public string Body { get; set; }
        public string Message { get; set; }
        public EmailTypeEnum emailType { get; set; }
        public string Personal_Email = "daniel@inspiredagency.co.uk";
        public string Personal_Email_Password = "1Dan2day2014";
        public string[] CC { get; set; }
        public string[] Recipients { get; set; }
        public string Host = "smtp.gmail.com";
  
    }
}
