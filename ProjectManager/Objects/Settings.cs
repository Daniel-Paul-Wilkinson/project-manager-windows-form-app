﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Objects
{
    [Serializable()]
    public class Settings
    {
        //account settings
        public string accountName { get; set; }
        public string accountImage { get; set; }

        //colours
        public string titleColour { get; set; }
        public string sidebarColour { get; set; }
        public string panelColourSecond { get; set; }
        public string panelColourPrimary { get; set; }


        //TODO: added this incase we need multipe setting objects - helps identify type - create enum.. 
        public string settingType = "Main";
        public string datasource { get; set; }

        //core datasource usage


        public Settings()
        {
        }

        public Settings(string accountName, string accountImage)
        {
            this.accountName = accountName;
            this.accountImage = accountImage;
        }
    }
}
