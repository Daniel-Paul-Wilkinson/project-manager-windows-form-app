﻿using ProjectManager.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ProjectManager.Objects
{
    [Serializable]
    public class Job
    {
        //id of a job within the database
        public int Id { get; set; }

        //id of the job within the company
        public string JobIdentifier { get; set; }

        //the name of the job
        public string Name { get; set; }

        public string URL { get; set; }
        public string Description { get; set; }
        public List<string> AuditTrail { get; set; }
        public JobProgressTypeEnum Progress { get; set; }
        public Job()
        {

        }
        public override string ToString()
        {
            return JobIdentifier + " - " + Name;
        }

    }
}
