﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Objects
{
    public class Report
    {
        public IEnumerable<dynamic> report { get; set; }
        public DateTime date { get; set; }
        public string name {get; set;}
        public int order { get; set; }

        public Report() { }

        public Report(IEnumerable<dynamic> report, DateTime date, string name, int order)
        {
            this.report = report;
            this.date = date;
            this.name = name;
            this.order = order;
        }

        public override string ToString()
        {
            return name;
        }
    }
}