﻿using ProjectManager.Enums;
using ProjectManager.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Interfaces
{
    public interface IProjectService
    {
        #region Project List CRUID
        bool Backup<T>(List<T> t, string datasource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON);
        bool Create<T>(List<T> t, string dataSource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON);
        bool Save<T>(List<T> t, string datasource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON);
        bool Populate<T>(T t, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON) where T : Type;
        dynamic Load<T>(T t, string dataSource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON) where T : Type;
        #endregion

        #region Project List Methods
        bool AddObjects<T>(List<T> objects);
        int GetCount<T>(T type) where T : class;
        dynamic GetAll<T>(T t) where T : Type;
        bool RemoveAll<T>(T t) where T : class;
        bool RemoveObjects<T>(T Obj);
        bool UpdateProject<T>(T oldObject, T newObject);
        bool ObjectExists<T>(T obj) where T : class;
        dynamic GetObjByFilter<T>(T t = null, string search = null) where T : Type;
        IEnumerable<dynamic> GetCompanyReport(Type type, string v);
        int maxId();
        dynamic getById<T>(T obj) where T : class;
        #endregion

            #region TestData
        void SeedTestDataFor<T>(T t) where T : Type;
        dynamic ReturnSeedData<T>(T t);
        
        #endregion
    }
}
