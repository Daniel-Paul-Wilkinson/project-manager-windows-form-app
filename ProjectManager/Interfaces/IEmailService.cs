﻿using ProjectManager.Enums;
using ProjectManager.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Interfaces
{
    interface IEmailService
    {
        void SendEmail(Email email);
        Email createEmailBody(Email email, Project project, Job job, EmailTypeEnum emailType);
    }
}
