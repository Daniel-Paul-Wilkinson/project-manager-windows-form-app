﻿namespace ProjectManager.Interfaces
{
    public interface IImageService
    {
        string GetThumbnail(string sourceUrl, int newWidth, int newHeight, string missingImageFile, bool fixMaxSize = false);
    }
}