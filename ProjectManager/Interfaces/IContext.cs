﻿using ProjectManager.Enums;
using ProjectManager.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Interfaces
{
    /* The idea is that this class is the main class that interacts with the data source and loads the data here.
     *  - Contains CRUID methods for the datasource 
     *  - Contains general file methods like generate a file name and getting the last backup.
     */

    public interface IContext
    {
        #region Data CRUID Methods
        dynamic Load<T>(T type, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON) where T : Type;
        bool Backup<T>(List<T> t = null, string dataSource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON);
        bool Save<T>(List<T> t = null, string dataSource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON);
        bool Create<T>(List<T> t = null, string dataSource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON);
        #endregion

        #region Test Data
        List<Project> SeedTestDataProjects();
        List<Repository> SeedTestDataRepository();
        List<Job> SeedTestDataJobs();
        List<Server> SeedTestDataServer();

        string getSaveFormat(DataSourceTypeEnum format);
        string SerializeTo<T>(List<T> list, DataSourceTypeEnum dataSourceTypeEnum = DataSourceTypeEnum.CSV);
        #endregion

        #region File Information
        object mapObjectProperites<T>(List<T> dataList);
        Type getObjectType<T>(T dataList);
        dynamic getObjectFromType(Type t);
        string GetFileExtension(string dataSource, string genericObjName, DataSourceTypeEnum dataSourceType);
        string GetFileSuffix(FileSuffixEnum fileSuffix = FileSuffixEnum.Daily);
        string GetBackupFileName(string Date, string backupDirectory = Consts.BackupDirectory, string backupNamePrefix = Consts.BackupFileNamePrefix, string backupNameSuffix = null, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON);
        string GetLastBackup();
        #endregion
    }
}
